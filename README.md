Asteroids Blasters!
============
This is a SEM group 54 project.

Have you ever wanted to go to space and shoot some asteroids :sparkler:? Well say no more, the ultimate asteroid blasting experience is just a few clicks away!

How to launch the game: :rocket:
1. Clone the repo. If you want the stable version, stay on master branch. Dare to try some crazy experiments and try new features? feel free to wander around the branches and take a look at what our development team is working on.
2. Inside the package  gui/states you will find a StartGUI Java class. launch it in order to play.

Please note that in order to play, you need to register.

Have :space_invader: fun playing! :video_game:

----
Found a bug? tell us about it in our GitLab issues and are developers will be right on it! 
