package game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import gui.AsteroidUI;
import gui.screens.PlayScreen;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("PMD.AvoidLiteralsInIfCondition")
public class Ship extends GameObject implements InputProcessor {
    private transient int originX;
    private transient int originY;
    private transient TextureRegion shipRegion;
    private transient int rotation;
    private transient int xval;
    private transient int yval;
    private transient int life;
    private transient List<Bullet> bullets = new ArrayList<>();
    private transient TextureRegion shipregion;
    private transient TextureRegion shipmoveregion;

    /**
     * get the bullet list.
     *
     * @return the bullet list.
     */
    public List<Bullet> getBullets() {
        return bullets;
    }

    /**
     * get the lives.
     *
     * @return the number of lives
     */
    public int getLives() {
        return life;
    }

    /**
     * dec the amount of lives.
     */
    public void decLife() {
        life--;
    }

    /**
     * Set the bullet list.
     *
     * @param bullets the list of bullets
     */
    public void setBullets(List<Bullet> bullets) {
        this.bullets = bullets;
    }

    /**
     * fires the bullets.
     *
     * @param b the bullet to fire.
     */
    public void fire(Bullet b) {
        b.setX(this.getBounds().x + this.getOriginX() - 10);
        b.setY(this.getBounds().y + this.getOriginY() - 10);
        double velX = getVelX();
        double velY = getVelY();
        b.setFaceAngle(this.getFaceAngle());

        double angle = this.getFaceAngle();

        b.setFaceAngle(getFaceAngle());
        b.setVelX(velX + (Math.cos(angle * Math.PI / 180)) * 2);
        b.setVelY(velY + (Math.sin(angle * Math.PI / 180)) * 2);
        bullets.add(b);
    }

    /**
     * test constructor.
     *
     * @param t the texture region
     */
    public Ship(TextureRegion t) {
        life = 2;
        xval = (AsteroidUI.WIDTH / 2) - (PlayScreen.SHIPSIZE / 2);
        yval = (AsteroidUI.HEIGHT / 2) - (PlayScreen.SHIPSIZE / 2);
        originX = PlayScreen.SHIPSIZE / 2;
        originY = PlayScreen.SHIPSIZE / 2;
        shipRegion = t;
        setAlive(true);
    }

    public void createTexture() {
        shipregion = new TextureRegion(new Texture("spaceship.png"));
        shipmoveregion = new TextureRegion(new Texture("spaceship2.png"));
    }

    /**
     * get the x coor. of the ship.
     *
     * @return the x value
     */
    public int getOriginX() {
        return originX;
    }

    /**
     * get the y coor. of the ship.
     *
     * @return the y value
     */
    public int getOriginY() {
        return originY;
    }

    /**
     * get the texture of the ship.
     *
     * @return the texture region
     */
    public TextureRegion getShip() {
        return shipRegion;
    }

    /**
     * get the rotation of the ship.
     *
     * @return the rot of the ship
     */
    public int getRotation() {
        return rotation;
    }

    /**
     * set the rotation.
     *
     * @param rotation the rotation
     */
    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    /**
     * Gets the Bounds of the rectangle around the ship.
     *
     * @return the Rectangle.
     */
    public Rectangle getBounds() {
        return new Rectangle((int) getxValue() - 6, (int) getY() - 6,
                PlayScreen.SHIPSIZE, PlayScreen.SHIPSIZE);
    }

    /**
     * set the texture region.
     *
     * @param region the textre region
     */
    public void setTexture(TextureRegion region) {
        shipRegion = region;
    }


    /**
     * anages jey presses.
     *
     * @param keycode the code of the key pressed
     * @return boolean if all input have been handled
     */
    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.RIGHT:
                incFaceAngle(-10);
                if (getFaceAngle() < 0) {
                    setFaceAngle(360 - 10);
                }
                break;
            case Input.Keys.LEFT:
                incFaceAngle(10);
                if (getFaceAngle() > 360) {
                    setFaceAngle(10);
                }
                break;
            case Input.Keys.DOWN:
                setMoveAngle(getFaceAngle() - 90);
                setVelX(getVelX()
                        + (Math.cos(getMoveAngle() * Math.PI / 180)) * 0.1);
                setVelY(getVelY()
                        + (Math.sin(getMoveAngle() * Math.PI / 180)) * 0.1);
                break;
            case Input.Keys.UP:
                setMoveAngle(getFaceAngle() - 90);
                setVelX(getVelX()
                        - (Math.cos(getMoveAngle() * Math.PI / 180)) * 0.1);
                setVelY(getVelY()
                        - (Math.sin(getMoveAngle() * Math.PI / 180)) * 0.1);
                this.setTexture(shipmoveregion);

                break;
            case Input.Keys.SPACE:
                Bullet b = new Bullet(
                        (Math.cos(getFaceAngle() * Math.PI / 180 + Math.PI / 2.05) * 5.7),
                        (Math.sin(getFaceAngle() * Math.PI / 180 + Math.PI / 2.05) * 5.7),
                        new TextureRegion(new Texture("bullet.png")));
                fire(b);
                break;
            default:
                return true;
        }
        return false;
    }

    /**
     * extra method from interface.
     *
     * @param keycode they keycode
     * @return boolean if all input have been handled
     */
    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.UP) {
            this.setTexture(shipregion);
            return true;
        }
        return false;

    }

    /**
     * extra method from interface.
     *
     * @param character they keycode
     * @return boolean if all input have been handled
     */
    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    /**
     * extra method from interface.
     *
     * @param screenX int
     * @param screenY int
     * @param pointer int
     * @param button  int
     * @return boolean if all input have been handled
     */
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    /**
     * extra method from interface.
     *
     * @param screenX int
     * @param screenY int
     * @param pointer int
     * @param button  int
     * @return boolean if all input have been handled
     */
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    /**
     * extra method from interface.
     *
     * @param screenX int
     * @param screenY int
     * @param pointer int
     * @return boolean if all input have been handled
     */
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    /**
     * extra method from interface.
     *
     * @param screenX int
     * @param screenY int
     * @return boolean if all input have been handled
     */
    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    /**
     * extra method from interface.
     *
     * @param amount int
     * @return boolean if all input have been handled
     */
    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    /**
     * update the location of the ship.
     *
     * @param deltaTime the update time
     */
    public void update(float deltaTime) {
        setX((getVelX() * deltaTime) * 10 + getxValue());
        setY((getVelY() * deltaTime) * 10 + getY());
    }
}
