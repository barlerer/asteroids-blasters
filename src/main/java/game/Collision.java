package game;


@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class Collision {
    private transient Ship ship;
    private transient Game game;
    private transient game.Intersector intersector;

    /**
     * Constructor for Collision class.
     * @param game The game to check for collisions
     */
    public Collision(Game game) {
        this.game = game;
        this.ship = game.getShip();
    }

    /**
     * Constructor for Collision class.
     * @param game The game to check for collisions
     */
    public Collision(Game game, game.Intersector intersector) {
        this.game = game;
        this.ship = game.getShip();
        this.intersector = intersector;
    }

    /**
     * Constructor for Collision class.
     * @param game The game to check for collisions
     */
    public Collision(Game game, game.Intersector intersector, Ship ship) {
        this.game = game;
        this.ship = ship;
        this.intersector = intersector;
    }

    /**
     * This class handles all the properties for collisions.
     *
     * @author SEM Group 54
     * @version 1.0
     * @since 1.0
     */
    public void checkCollisions() {
        //if statement for 2 fixtures check 4 types and call the methods
        //loop over asteroids
        int l = 0;
        for (int i = 0; i < game.getAsteroids().size(); i++) {
            Asteroid a = game.getAsteroids().get(i);
            if (intersector.intersect(ship.getBounds(), a.getBounds())) {
                ship.decLife();
                a.setAlive(false);

                game.getAsteroids().remove(i);

                if (ship.getLives() == 0) {
                    ship.setAlive(false);
                    game.gameEnd = true;
                }
                game.getShip().setX(420);
                game.getShip().setY(280);
            }

            for (int j = 0; j < ship.getBullets().size(); j++) {
                Bullet b = ship.getBullets().get(j);
                if (intersector.intersect(a.getBounds(), b.getBounds())) {
                    game.soundStart();
                    game.getAsteroids(). remove(a);
                    game.split(a, a.getAsteroidRegion());
                    ship.getBullets().remove(b);
                    int x = (int) (game.getShip().getxValue() - b.getxValue());
                    int y = (int) (game.getShip().getY() - b.getY());
                    game.addScore(Math.ceil(Math.sqrt(x * x + y * y) / 20));
                }
            }
        }

    }
}
