package game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

/**
 * This class handles all the properties of bullets.
 *
 * @author SEM Group 54
 * @version 1.0
 * @since 1.0
 */

public class Bullet extends GameObject {
    private transient double dirX;
    private transient double dirY;
    private transient TextureRegion bulletRegion;


    /**
     * This makes the bullet shape.
     */
    public Rectangle getBounds() {
        return new Rectangle((int) getxValue() - 1, (int) getY() - 1, 1, 1);
    }

    /**
     * the bullet test constructor.
     * @param directionX the x direction of the bullet
     * @param directionY the y direction of the bullet
     * @param t the texture region
     */
    public Bullet(double directionX, double directionY, TextureRegion t) {
        setShape(new Polygon(new float[] {0, 0, 0, 0, 1, 1, 1, 1}));
        setAlive(false);
        this.dirX = directionX;
        this.dirY = directionY;
        bulletRegion = t;
    }

    /**
     * get the xDir of the bullet.
     * @return the x value
     */
    public double getDirX() {
        return dirX;
    }

    /**
     * get the yDir of the bullet.
     * @return the y value
     */
    public double getDirY() {
        return dirY;
    }

    /**
     * get the texture region of the bullet.
     * @return the x value
     */
    public TextureRegion getBulletRegion() {
        return bulletRegion;
    }

    /**
     * set the y dir.
     * @param dirY the die to set it to
     */
    public void setDirY(double dirY) {
        this.dirY = dirY;
    }

    /**
     * set the x dir.
     * @param dirX the die to set it to
     */
    public void setDirX(double dirX) {
        this.dirX = dirX;
    }

}


