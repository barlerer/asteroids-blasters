package game;

import com.badlogic.gdx.math.Polygon;

public class GameObject {


    /**
     * This class handles all the properties which are in common with asteroid,
     * bullet and ship.
     *
     * @author SEM Group 54
     * @version 1.0
     * @since 1.0
     */

    private Polygon shape;
    private boolean alive;
    private transient double posX;
    private transient double posY;
    private transient double velX;
    private transient double velY;
    private double moveAngle;
    private double faceAngle;


    /**
     * get the shape.
     *
     * @return the shape
     */
    public Polygon getShape() {
        return shape;
    }

    /**
     * Get the status of the ship.
     *
     * @return the status
     */
    public boolean isAlive() {
        return alive;
    }

    /**
     * get the xValue.
     *
     * @return the xValue
     */
    public double getxValue() {
        return posX;
    }

    /**
     * get the Y value.
     *
     * @return the y value
     */
    public double getY() {
        return posY;
    }

    /**
     * get the x velocity.
     *
     * @return the x velocity
     */
    public double getVelX() {
        return velX;
    }

    /**
     * get the y velocity.
     *
     * @return the y velocity
     */
    public double getVelY() {
        return velY;
    }

    /**
     * get the move angle.
     *
     * @return the move angle
     */
    public double getMoveAngle() {
        return moveAngle;
    }

    /**
     * get the face angle.
     *
     * @return
     */
    public double getFaceAngle() {
        return faceAngle;
    }

    /**
     * set the shape.
     *
     * @param shape the shape
     */
    public void setShape(Polygon shape) {
        this.shape = shape;
    }

    /**
     * set the status.
     *
     * @param alive the status
     */
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    /**
     * set the  x.
     *
     * @param posX x to change to
     */
    public void setX(double posX) {
        this.posX = posX;
    }

    /**
     * Inc the  X.
     *
     * @param i the amount to increment by
     */
    public void incX(double i) {
        this.posX += i;
    }

    /**
     * Set the  Y.
     *
     * @param y the pos
     */
    public void setY(double y) {
        this.posY = y;
    }

    /**
     * Inc the  Y.
     *
     * @param i the amount to increment by
     */
    public void incY(double i) {
        this.posY += i;
    }

    /**
     * set the vel X.
     *
     * @param velX the amount to increment by
     */
    public void setVelX(double velX) {
        this.velX = velX;
    }

    /**
     * Set the vel Y.
     *
     * @param velY the amount to increment by
     */
    public void setVelY(double velY) {
        this.velY = velY;
    }

    /**
     * Set the face angle.
     *
     * @param angle the amount to increment by
     */
    public void setFaceAngle(double angle) {
        this.faceAngle = angle;
    }

    /**
     * Increment the face angle.
     *
     * @param i the amount to increment by
     */
    public void incFaceAngle(double i) {
        this.faceAngle += i;
    }

    /**
     * Set the move angle.
     *
     * @param angle the angle
     */
    public void setMoveAngle(double angle) {
        this.moveAngle = angle;
    }

    /**
     * Increment the move angle.
     *
     * @param i the amount to increment by
     */
    public void incMoveAngle(double i) {
        this.moveAngle += i;
    }
}
