package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import gui.AsteroidUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings({"PMD.AvoidLiteralsInIfCondition", "PMD.DataflowAnomalyAnalysis"})
public class Game extends com.badlogic.gdx.Game {

    private transient Ship ship;
    private transient World world;
    private transient int score;
    private transient Random rand = new Random();
    private transient List<Asteroid> asteroids = new ArrayList<>();
    public transient Boolean gameEnd = false;
    private transient Sound sound;
    private transient int level = 0;
    private transient Collision collision;
    private transient int size = 1;

    public TextureRegion getShipregion() {
        return shipregion;
    }

    public TextureRegion getShipmoveregion() {
        return shipmoveregion;
    }

    private transient TextureRegion shipregion;
    private transient TextureRegion shipmoveregion;


    private transient int numAsteroids = 0;
    private transient TextureRegion textureRegionAst;


    /**
     * Getter for size.
     * @return The size
     */
    public int getSize() {
        return size;
    }

    /**
     * Setter for size.
     * @param size The new size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Constructor for game.
     */
    public Game() {
        score = 0;
    }

    /**
     * returns asteroid list.
     *
     * @return the list.
     */
    public List<Asteroid> getAsteroids() {
        return asteroids;
    }

    /**
     * set the asteroid list.
     *
     * @param as the list.
     */
    public void setAsteroids(List<Asteroid> as) {
        asteroids = as;
    }

    /**
     * get the score.
     *
     * @return score
     */
    public int getScore() {
        return score;
    }

    /**
     * adds 10 to the score.
     */
    public void addScore(double factor) {
        this.score += 100 / factor;
    }

    /**
     * Getter for level.
     * @return The level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Starts sound effects.
     */
    public void soundStart() {
        sound = Gdx.audio.newSound(Gdx.files.internal("music/shot.mp3"));
        sound.play(1.0f);
    }

    /**
     * Ends soundeffects.
     */
    public void soundEnd() {
        sound.stop();
        sound.dispose();

    }

    /**
     * Setter for the ship.
     * @param s the ship
     */
    public void setShip(Ship s) {
        ship = s;
    }

    @Override
    public void create() {
        textureRegionAst = new TextureRegion(new Texture("asteroid.png"));
        shipregion =  new TextureRegion(new Texture("spaceship.png"));
        shipmoveregion = new TextureRegion(new Texture("spaceship2.png"));
        ship = new Ship(shipregion);
        ship.createTexture();

        initShip(ship);
        createWorld();
        Intersector intersector = new Intersector();
        collision = new Collision(this, intersector);
        for (int i = 0; i < 10; i++) {
            Asteroid ast = new Asteroid(textureRegionAst);
            initAst(ast);
        }
        newLevel(textureRegionAst);
    }

    /**
     * initializes the ship to middle of the screen.
     */
    public void initShip(Ship ship) {
        ship.setX(AsteroidUI.WIDTH / 2);
        ship.setY(AsteroidUI.HEIGHT / 2);

    }

    /**
     * initialize teh asteroid.
     *
     * @param ast teh asteroid to init.
     */
    public void initAst(Asteroid ast) {
        do {
            ast.setY((double) rand.nextInt(AsteroidUI.HEIGHT) - 40);
            ast.setX((double) rand.nextInt(AsteroidUI.WIDTH) - 40);
        } while ((ast.getY() < 380 && ast.getY() > 180)
            || (ast.getxValue() < 520 && ast.getxValue() > 320));

        ast.setMoveAngle(rand.nextInt(360));
        ast.setRotationVelocity(rand.nextInt(3) + 1);
        double ang = ast.getMoveAngle() - 90;
        ast.setVelX(Math.cos(ang * Math.PI / 180));
        ast.setVelY(Math.sin(ang * Math.PI / 180));
        asteroids.add(ast);
    }


    /**
     * creates world.
     */
    public void createWorld() {
        world = new World(new Vector2(0, 0), false);
    }

    /**
     * Updates positions of game elements according to delta time.
     *
     * @param dt Delta time.
     */
    public void gameUpdate(float dt) {
        ship.update(dt);
        wrapAround(ship);
        bulletUpdate(dt);
        collision.checkCollisions();
        newLevel(textureRegionAst);
        astUpdate(dt);
    }

    /**
     * Starts new level.
     */
    public void newLevel(TextureRegion textureRegionAst) {
        if (asteroids.isEmpty()) {
            level++;
            if (numAsteroids <= 40) {
                numAsteroids += 10;
            }
            for (int i = 0; i < numAsteroids; i++) {
                Asteroid ast = new Asteroid(textureRegionAst);
                initAst(ast);
            }
        }
    }

    /**
     * updates the location of the bullet.
     *
     * @param deltaTime the time frame
     */
    public void bulletUpdate(float deltaTime) {
        for (int i = 0; i < ship.getBullets().size(); i++) {
            Bullet b = ship.getBullets().get(i);
            if (b.getxValue() > AsteroidUI.WIDTH || b.getY() > AsteroidUI.HEIGHT
                || b.getxValue() < 0 || b.getY() < 0) {
                ship.getBullets().remove(i);
            }
            b.setX((b.getDirX() * deltaTime) * 57 + b.getxValue());
            b.setY((b.getDirY() * deltaTime) * 57 + b.getY());
        }
    }

    /**
     * method to make the objects wrap around.
     * @param obj to object to update
     */
    public void wrapAround(GameObject obj) {
        if (AsteroidUI.WIDTH - obj.getxValue() <= 0) {
            obj.setX(obj.getxValue() - AsteroidUI.WIDTH);
        }
        if (AsteroidUI.WIDTH - obj.getxValue() >= AsteroidUI.WIDTH) {
            obj.setX(obj.getxValue() + AsteroidUI.WIDTH);
        }
        if (AsteroidUI.HEIGHT - obj.getY() <= 0) {
            obj.setY(obj.getY() - AsteroidUI.HEIGHT);
        }
        if (AsteroidUI.HEIGHT - obj.getY() >= AsteroidUI.HEIGHT) {
            obj.setY(obj.getY() + AsteroidUI.HEIGHT);
        }
    }

    /**
     * updates the location of the asteroids.
     *
     * @param deltaTime the time frame
     */
    public void astUpdate(float deltaTime) {
        for (int i = 0; i < asteroids.size(); i++) {
            Asteroid a = asteroids.get(i);
            a.setX((a.getVelX() * deltaTime) * 10 + a.getxValue());
            a.setY((a.getVelY() * deltaTime) * 10 + a.getY());
            wrapAround(a);
        }

    }

    /**
     * method to split the asteroids.
     * @param a the asteroid to split
     * @param t the texture region of the the asteroid
     */
    public void split(Asteroid a, TextureRegion t) {
        a.decSplitCount();
        if (a.getSplitCount() > 0) {
            for (int i = 0; i < 2; i++) {
                Asteroid as = new Asteroid(t);
                as.setMoveAngle(rand.nextInt(360));
                as.setVelX(Math.sin((as.getMoveAngle() - 90) * Math.PI / 180));
                as.setVelY(Math.cos((as.getMoveAngle() - 90) * Math.PI / 180));
                as.setY(a.getY());
                as.decSplitCount();
                asteroids.add(as);
            }
            asteroids.get(asteroids.size() - 1).setX(a.getxValue() + 20);
            asteroids.get(asteroids.size() - 1).setX(a.getxValue() - 20);
        }
    }

    /**
     * Getter for the ship.
     * @return The ship
     */
    public Ship getShip() {
        return ship;
    }

    /**
     * Getter for the world.
     * @return The world
     */
    public World getWorld() {
        return world;
    }
}