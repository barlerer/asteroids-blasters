package game;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;

import com.badlogic.gdx.math.Rectangle;

/**
 * This class handles all the variables for asteroid class.
 *
 * @author SEM Group 54
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class Asteroid extends GameObject {
    private transient TextureRegion asteroidRegion;
    private transient int splitCount;
    private transient double rotVel;

    /**
     * gets the texture region.
     * @return the texture region
     */
    public double getRotationVelocity() {
        return rotVel;
    }

    /**
     * set the rotVel.
     * @param v vel to set it to
     */
    public void setRotationVelocity(double v) {
        rotVel = v;
    }

    /**
     * dec the split count.
     */
    public void decSplitCount() {
        this.splitCount--;
    }

    /**
     * gets the splitcount.
     * @return the texture region
     */
    public int getSplitCount() {
        return splitCount;
    }


    /**
     * This makes the bounding boxes.
     *
     * @author SEM Group 54
     * @version 1.0
     * @since 1.0
     */
    public Rectangle getBounds() {
        return new Rectangle((int) getxValue() - (30 * splitCount),
            (int) getY() - (30 * splitCount), (60 * splitCount), (60 * splitCount));
    }

    /**
     * the constructor for Asteroids.
     */
    public Asteroid(TextureRegion t) {
        splitCount = 2;
        setAlive(true);
        this.rotVel = (0.0);
        this.asteroidRegion = t;
    }

    /**
     * gets the texture region.
     * @return the texture region
     */
    public TextureRegion getAsteroidRegion() {
        return asteroidRegion;
    }

    /**
     * set the texture region.
     * @param t the asteroid region
     */
    public void setAsteroidRegion(TextureRegion t) {
        this.asteroidRegion = t;
    }

}
