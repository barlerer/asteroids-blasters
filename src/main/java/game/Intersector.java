package game;

import com.badlogic.gdx.math.Rectangle;

/**
 * Intersector class wrapper, so we can mock.
 */
public class Intersector {
    /**
     * Checks if 2 triangles are colliding.
     * @param r1 First triangle
     * @param r2 Second triangle
     * @return true if intersect, false otherwise
     */
    public boolean intersect(Rectangle r1, Rectangle r2) {
        return com.badlogic.gdx.math.Intersector.overlaps(r1, r2);
    }
}
