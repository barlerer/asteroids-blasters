package database.connection;

public interface ScoreInterface {

    int getPoints();

    void setPoints(int points);

    String getNickname();

    void setNickname(String nickname);
}
