package database.connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Parses the results coming back from databases.
 * @author Bar Lerer
 * @author SEM-54
 * @version 1.0
 * @since 1.0
 */
public class DatabaseParser {

    /**
     * Parses ResultSet that is coming from user login.
     * @param rs ResultSet that comes from the database
     * @return The parsed user from database
     */
    public User parseLogin(ResultSet rs) {
        try {
            rs.first();
            String email = rs.getString(1);
            String password = rs.getString(2);
            User user = new User(email, password);
            user.setRegistered(true);
            rs.close();
            return user;
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Parses scores from the database.
     * @param executeQuery The resultSet to parse
     * @return ArrayList of parsed scores
     */
    public ArrayList<Score> parseScores(ResultSet executeQuery) {
        ArrayList<Score> scores = new ArrayList<>();
        try {
            while (executeQuery.next()) {
                Score score = new Score(executeQuery.getInt("score"));
                score.setNickname(executeQuery.getString("nickname"));
                scores.add(score);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return scores;
    }

    /**
     * Parses user id from DB.
     * @param executeQuery The resultset from db
     * @return The user id, -1 if not found
     */
    public int parseUserId(ResultSet executeQuery) {
        try {
            executeQuery.first();
            return executeQuery.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
