package database.connection;

import java.util.ArrayList;

/**
 * The user.
 * @author Bar Lerer
 * @author SEM-54
 * @version 1.0
 * @since 1.0
 */
public class User implements UserInterface {
    private String email;
    private String password;
    private boolean registered;
    private ArrayList<Score> scores;

    /**
     * Creates a user.
     * @param email Email address of the user
     * @param password Password of user
     */
    public User(String email, String password) {
        this.email = email;
        this.password = password;
        scores = new ArrayList<>();
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isRegistered() {
        return registered;
    }

    @Override
    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    @Override
    public ArrayList<Score> getScores() {
        return scores;
    }

    @Override
    public void setScores(ArrayList<Score> scores) {
        this.scores = scores;
    }

    @Override
    public void addScore(Score score) {
        this.scores.add(score);
    }

    public void cryptPassword() {
        Hashing hashing = new Hashing();
        setPassword(hashing.crypt(getPassword()));
    }

    public boolean verifyCrypt(String password) {
        Hashing hashing = new Hashing();
        return hashing.verifyCrypt(password, getPassword());
    }
}
