package database.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Connects to the database.
 * @author Bar Lerer
 * @author SEM-54
 * @version 1.0
 * @since 1.0
 */
public class DatabaseAccess {

    private transient Connection connection;
    private static DatabaseParser databaseParser = new DatabaseParser();

    /**
     * Creates a connection to the DB.
     * @param url URL of database
     * @param user User of database
     * @param password Password of database
     * @return The connection object to DB.
     */
    public Connection connectToDB(String url, String user, String password) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            connection = DriverManager.getConnection(url, user, password);

            return connection;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * Handles registration of user to the database. Creates a
     * prepared statement and sends it to the database.
     * @param user The user to register
     * @return True if the user was registered successfully
     */
    public boolean registerUser(User user) {
        try {
            PreparedStatement statement = connection
                    .prepareStatement("INSERT INTO users (email, password) VALUES (?, ?);");
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.execute();
            user.setRegistered(true);
            return true;
        } catch (SQLException e) {
            System.out.println("database.connection.User is already registered");
        }
        return false;
    }

    /**
     * Handles user login.
     * @param user The user that wants to login
     * @return True if user was authenticated
     */
    public boolean userLogin(User user) {
        try {
            PreparedStatement statement = connection
                    .prepareStatement("SELECT email, password FROM users WHERE email = ?;");
            statement.setString(1, user.getEmail());

            DatabaseParser databaseParser = new DatabaseParser();
            User userFromDB = databaseParser.parseLogin(statement.executeQuery());
            if (userFromDB != null) {
                if  (userFromDB.verifyCrypt(user.getPassword())) {
                    getScores(user);
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Updates scores field of a user.
     * @param user The user of which we want to get the scores
     */
    public void getScores(User user) {
        try {
            PreparedStatement statement = connection
                    .prepareStatement("Select id, email, score, nickname from users "
                            + "INNER JOIN scores on user_id=id "
                            + "WHERE email = ? ORDER BY score DESC;");
            statement.setString(1, user.getEmail());
            ArrayList<Score> scores = databaseParser.parseScores(statement.executeQuery());
            user.setScores(scores);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves a new score for a user.
     * @param user The user we want to save the score to
     * @param nickname The nickname the user has choose
     * @param score The score we want to save
     * @return True if score was saved
     */
    public boolean saveScore(User user, String nickname, Score score) {
        if (user == null || nickname == null || nickname.isEmpty() || score == null) {
            return false;
        }
        int userId = getUserId(user);
        if (userId != -1) {
            try {
                PreparedStatement ps = connection
                        .prepareStatement("INSERT INTO "
                                + "scores (user_id, score, nickname) VALUES (?, ?, ?);");
                ps.setInt(1, userId);
                ps.setInt(2, score.getPoints());
                ps.setString(3, nickname);
                ps.execute();
                getScores(user);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Setter for connection.
     * @param connection The connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Gets the user if from the DB.
     * @param user The user of whom we want to get the id
     * @return The user id, -1 if not found
     */
    public int getUserId(User user) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT id,email from users where email = ?;");
            ps.setString(1, user.getEmail());
            return databaseParser.parseUserId(ps.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Getting the top 10 scores from the database.
     * @return List with 10 top scores
     */
    @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
    public List<Score> getTopScores() {
        List<Score> scores = new ArrayList<>();
        try {
            PreparedStatement ps = connection
                    .prepareStatement("Select score, nickname "
                            + "from users INNER JOIN scores on user_id=id "
                            + "where score!=0 ORDER BY score DESC Limit 10;");
            ps.addBatch();

            scores = databaseParser.parseScores(ps.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return scores;
    }
}