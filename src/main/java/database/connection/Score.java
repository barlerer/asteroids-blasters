package database.connection;

/**
 * This class handles points object.
 * @author Bar Lerer
 * @author SEM-54
 * @version 1.0
 * @since 1.0
 */

public class Score implements ScoreInterface {

    private int points;
    private String nickname;

    /**
     * Constructor with points and nickname of user.
     *
     * @param points   The points
     * @param nickname The nickname
     */
    public Score(int points, String nickname) {
        this.points = points;
        this.nickname = nickname;
    }

    /**
     * Constructor for points.
     *
     * @param points The points
     */
    public Score(int points) {
        this.points = points;
    }

    /**
     * Constructor without points.
     */
    public Score() {
        this.points = 0;
    }

    /**
     * Getter for the points.
     *
     * @return The points
     */
    @Override
    public int getPoints() {
        return points;
    }

    /**
     * Setter for the points.
     *
     * @param points The new points.
     */
    @Override
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Getter for nickname.
     *
     * @return The nickname.
     */
    @Override
    public String getNickname() {
        return this.nickname;
    }

    /**
     * Setter for nickname.
     *
     * @param nickname The new nickname to set
     */
    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

}
