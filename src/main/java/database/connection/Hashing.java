package database.connection;

import at.favre.lib.crypto.bcrypt.BCrypt;

/**
 * This class handles hashing and verifying passwords.
 * @author Bar Lerer
 * @author SEM Group 54
 * @version 1.0
 * @since 1.0
 */

public class Hashing {

    public Hashing() {
    }

    /**
     * Creates hashed string + salt to the incoming password.
     * @param password The password to be hashed
     * @return A string value of the hashed value
     */
    public String crypt(String password) {
        if (password == null) {
            return null;
        }
        String bcryptHashString = BCrypt.withDefaults().hashToString(6, password.toCharArray());
        return bcryptHashString;
    }

    /**
     *  Check if the password and the hash value match.
     * @param password The password the method will check
     * @param crypt The hash value of the password
     * @return boolean value if the passwords match
     */
    public boolean verifyCrypt(String password, String crypt) {
        if (password == null) {
            return false;
        }
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), crypt);
        return result.verified;
    }
}
