package database.connection;

import java.util.ArrayList;

public interface UserInterface {
    String getEmail();

    void setEmail(String email);

    String getPassword();

    void setPassword(String password);

    boolean isRegistered();

    void setRegistered(boolean registered);

    ArrayList<Score> getScores();

    void setScores(ArrayList<Score> scores);

    void addScore(Score score);
}
