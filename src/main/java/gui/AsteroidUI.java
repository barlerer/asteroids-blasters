package gui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import database.connection.User;
import gui.screens.LoginScreen;
import gui.screens.PlayScreen;

/**
 * The main GUI Game class.
 */
public class AsteroidUI extends Game {
    public static final int WIDTH = 840;
    public static final int HEIGHT = 560;
    public static final int STD_BTN_HEIGHT = 60;
    public static final int STD_BTN_WIDTH = 300;

    public static final String TITLE = "Asteroids";

    private static Controller controller;
    private static User currentUser = null;
    private static SpriteBatch batch;
    private static BitmapFont font;
    private transient Music backgroundMusic;
    public transient Boolean musicPlaying = false;


    /**
     * Entrance point to the GUI, sets the first screen.
     */
    @Override
    public void create() {
        controller = new Controller();
        batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.RED);

        this.setScreen(new LoginScreen(this));
    }

    /**
     * Disposes of class elements.
     */
    @Override
    public void dispose() {
        try {
            controller.getConnection().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        batch.dispose();
        font.dispose();
    }

    /**
     * Calls parent rendering method.
     */
    @Override
    public void render() {
        super.render();
    }

    /**
     * Resizes the screen.
     * @param width The width.
     * @param height The height.
     */
    @Override
    public void resize(int width, int height) {
    }

    /**
     * Pauses the game.
     */
    @Override
    public void pause() {
    }

    /**
     * Resumes the game.
     */
    @Override
    public void resume() {
    }

    /**
     * Returns current logged in user.
     * @return The user.
     */
    public User getUser() {
        return currentUser;
    }

    /**
     * Sets the logged in user.
     *
     * @param user the user which has logged in.
     * @return the User.
     */
    public User setUser(User user) {
        if (currentUser == null) {
            currentUser = user;
        } else {
            return null;
        }
        return user;
    }


    /**
     * Returns the DB controller.
     * @return the Controller.
     */
    public Controller getController() {
        return controller;
    }

    /**
     * Starts background music.
     */
    public void musicStart() {
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("music/background.mp3"));
        backgroundMusic.setLooping(true);
        backgroundMusic.play();
        musicPlaying = true;
    }

    /**
     * Ends background music.
     */
    public void musicEnd() {
        backgroundMusic.stop();
        backgroundMusic.dispose();
        musicPlaying = false;


    }

}
