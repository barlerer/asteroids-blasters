package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import gui.AsteroidUI;

/**
 * This class shows a screen of instructions of the flow of the game.
 */
public class InstructionScreen extends BasicScreen implements Screen {

    private transient TextButton backbutt;
    private transient Label controls;
    private transient Label flow;


    /**
     * The instruction screen in the app.
     *
     * @param game Shows instructions to play the game.
     *             It initializes the UI parts, as in labels and buttons.
     */
    public InstructionScreen(AsteroidUI game) {
        super(game);

        makeBackButt();
        makeLabels();

        stage.addActor(backbutt);
        stage.addActor(flow);
        stage.addActor(controls);
    }


    /**
     * It creates the back button.
     */
    private void makeBackButt() {
        backbutt = new TextButton("Back", skin);
        backbutt.setPosition((float) (AsteroidUI.WIDTH / 57), (float) (AsteroidUI.HEIGHT / 1.14));
        backbutt.setSize(100, 60);
        backbutt.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                backAction();
            }
        });
    }

    /**
     * It creates the instruction labels.
     */
    private void makeLabels() {
        controls = new Label("Move forwards: arrow key up\n"
                + "Move back: arrow key down\n"
                + "Rotate left: arrow key left\n"
                + "Rotate right: arrow key right\n"
                + "Shoot: spacebar", skin);
        controls.setPosition(AsteroidUI.WIDTH - 800, AsteroidUI.HEIGHT - 250);

        flow = new Label("In Asteroids Blaster you "
                + "play as a space ship and your "
                + "goal is to destroy all the as"
                + "teroids flying\nthrough space. Try to gain as many points"
                + "by shooting the bullets"
                + " from your ship into the\norbiting asteroids."
                + "Make sure you stay away from"
                + " the asteroids with your ship since a hit will fatal.\n"
                + "You can progress through "
                + "the game by clearing all asteroids on the screen, this will put you \n"
                + "into the next level where"
                + " you will need to survive in "
                + "even more difficult circumstances.\n"
                + "The game will only end when "
                + "you die or decide to chose the easy way out,\n"
                + "so try to get as far as you "
                + "can and share your score on the leaderboards.", skin);
        flow.setPosition(AsteroidUI.WIDTH - 800, AsteroidUI.HEIGHT - 420);
    }

    /**
     * This method takes you to the main menu.
     */
    private void backAction() {
        game.setScreen(new MenuScreen(game));
        super.dispose();
    }

    /**
     * It renders the instruction screen.
     *
     * @param delta gives the interval of the calls of the method.
     *              The method draws headers, stage and background.
     */
    @Override
    public void render(float delta) {
        SpriteBatch sb = new SpriteBatch();
        sb.begin();
        sb.draw(background, 0, 0, stage.getWidth(), stage.getHeight());
        font.draw(sb, "Controls",
                (AsteroidUI.WIDTH - 800),
                (int) (AsteroidUI.HEIGHT / 1.25));
        font.draw(sb, "Game flow",
                (AsteroidUI.WIDTH - 800),
                (int) (AsteroidUI.HEIGHT / 1.8));
        font.draw(sb, "Instructions",
                (int) (AsteroidUI.WIDTH / 2 - 3.6 * font.getData().capHeight),
                (int) (AsteroidUI.HEIGHT / 1.10));

        sb.end();
        stage.act();
        stage.draw();
    }


}
