package gui.screens;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.Asteroid;
import game.Bullet;
import game.Game;
import game.Ship;
import gui.AsteroidUI;

import java.util.List;


/**
 * PlayScreen class - main state where the game happens.
 */
@SuppressWarnings({"PMD.AvoidLiteralsInIfCondition", "PMD.DataflowAnomalyAnalysis",
    "NullAssignment"})
public class PlayScreen extends BasicScreen implements Screen, Runnable {


    public static final int SHIPSIZE = 50;
    private transient Game game;
    private transient boolean isPaused;
    private transient AsteroidUI asteroidGame;
    private transient float inputTime;
    private transient BitmapFont font;
    private transient Thread gameloop;
    private transient float dt;


    private transient Ship ship;
    private transient List<Asteroid> asteroids;

    /**
     * The Play screen which controls the game.
     *
     * @param asteroidGame the Game GUI.
     */
    public PlayScreen(AsteroidUI asteroidGame) {
        super(asteroidGame);
        this.asteroidGame = asteroidGame;
        game = new Game();
        game.create();
        ship = game.getShip();
        asteroids = game.getAsteroids();
        stage = new Stage();
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(game.getShip());
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);
        initButtons();
        initScore();
        start();

    }

    /**
     * This constructor is used when we want to resume a game.
     *
     * @param asteroidGame The Game GUI.
     * @param game         The previous Game.
     */
    public PlayScreen(AsteroidUI asteroidGame, Game game) {
        super(asteroidGame);
        this.asteroidGame = asteroidGame;
        this.game = game;
        ship = game.getShip();
        asteroids = game.getAsteroids();
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(game.getShip());
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);
        initButtons();
        initScore();
        start();
    }

    /**
     * Initializes the buttons.
     */
    private void initButtons() {
        TextButton pausebtn = new TextButton("Pause", skin);
        pausebtn.setPosition((int) (AsteroidUI.WIDTH / 1.15), (int) (AsteroidUI.HEIGHT / 1.14));
        pausebtn.setSize(100, 60);
        pausebtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                pause();
            }
        });
        stage.addActor(pausebtn);

    }

    /**
     * Changes screen to death screen.
     */
    private void gameEnd() {
        asteroidGame.setScreen(new DeathScreen(asteroidGame, game));
        dispose();
    }

    /**
     * Renders the elements on the screen.
     * @param delta the delta time of the game.
     */
    @Override
    public void render(float delta) {
        dt = delta;
        renderElements(delta);
        if (isPaused) {
            delta = 0;
        }
        game.gameUpdate(delta);
        if (game.gameEnd) {
            gameEnd();
        }

    }

    /**
     * Rendering helper method.
     * @param delta The delta time.
     */
    private void renderElements(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        inputTime += delta;
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            game.getShip().keyDown(Input.Keys.LEFT);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            game.getShip().keyDown(Input.Keys.RIGHT);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            game.getShip().keyDown(Input.Keys.UP);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            game.getShip().keyDown(Input.Keys.DOWN);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            if (inputTime > 0.4f) {
                game.getShip().keyDown(Input.Keys.SPACE);
                inputTime = 0;
            }

        }
        SpriteBatch sb = new SpriteBatch();
        sb.begin();
        sb.draw(background, 0, 0, AsteroidUI.WIDTH, AsteroidUI.HEIGHT);
        sb.end();
        stage.act();
        stage.draw();
        drawElements(sb);
    }

    /**
     * Pauses the game, changes screen to death screen.
     */
    @Override
    public void pause() {
        isPaused = !isPaused;
        Game game = this.game;
        asteroidGame.setScreen(new PauseScreen(asteroidGame, game));
        dispose();
    }

    /**
     * Resumes game.
     */
    @Override
    public void resume() {
        isPaused = false;
        initButtons();
    }

    /**
     * Disposes elements.
     */
    @Override
    public void dispose() {
        game.dispose();
        stop();
        super.dispose();
    }

    /**
     * Draws all elements of the game using a sprite batch.
     *
     * @param sb The Sprite batch.
     */
    private void drawElements(SpriteBatch sb) {
        sb.begin();
        for (Bullet bullet : ship.getBullets()) {
            sb.draw(bullet.getBulletRegion(), bullet.getBounds().x, bullet.getBounds().y,
                    bullet.getBounds().width / 2, bullet.getBounds().height / 2,
                    bullet.getBounds().width * 10, bullet.getBounds().height * 10, 1, 1,
                    (float) bullet.getFaceAngle());
        }
        sb.draw(ship.getShip(), ship.getBounds().x, ship.getBounds().y,
                ship.getOriginX(), ship.getOriginY(), ship.getBounds().width,
                ship.getBounds().height, 1, 1, (float) ship.getFaceAngle());
        for (Asteroid ast : asteroids) {
            sb.draw(ast.getAsteroidRegion(), ast.getBounds().x, ast.getBounds().y,
                    ast.getBounds().height / 2, ast.getBounds().height / 2,
                    ast.getBounds().width, ast.getBounds().height, 1,
                    1, (float) ast.getFaceAngle());

        }
        font.draw(sb, "Score: " + game.getScore(),
                (float) (AsteroidUI.WIDTH / 57), (float) (AsteroidUI.HEIGHT / 1.05));
        sb.end();
    }

    /**
     * initialize score.
     */
    private void initScore() {
        skin = new Skin(Gdx.files.internal("skin/neon-ui.json"));
        font = skin.getFont("font-over");
        font.getData().scale((float) 0.6);
    }

    /*
     * Stops thread.
     */
    @SuppressWarnings("PMD.NullAssignment")
    private void stop() {
        //kill the gameloop thread
        gameloop.interrupt();
    }

    /**
     * Starts thread.
     */
    private void start() {
        //create the gameloop thread for real-time updates
        gameloop = new Thread(this);
        gameloop.start();

    }

    /**
     * Starts game running with thread.
     */
    @Override
    public void run() {
        //acquire the current thread
        Thread t = Thread.currentThread();
        //keep going as long as the thread is alive
        while (t.equals(gameloop)) {
            try {
                //update the game loop
                game.gameUpdate(dt);
                //target framerate is 5fps
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
