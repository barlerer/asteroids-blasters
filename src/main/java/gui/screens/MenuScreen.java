package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import gui.AsteroidUI;
import gui.Controller;


/**
 * This class handles the Menu screen of the app.
 */
public class MenuScreen extends BasicScreen implements Screen {

    /**
     * The menu screen in the app.
     *
     * @param game The AsteroidUI instance.
     *             It initializes all of the UI parts, as in buttons background and error messages.
     */
    public MenuScreen(AsteroidUI game) {
        super(game);

        TextButton playbtn = new TextButton("Play", skin);
        playbtn.setPosition(middle, 330);
        playbtn.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        playbtn.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToPlay();
            }
        });

        TextButton settings = new TextButton("Settings", skin);
        settings.setPosition(middle, 190);
        settings.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        settings.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToSettings();
            }
        });

        TextButton mygamesbtn = new TextButton("Leaderboards", skin);
        mygamesbtn.setPosition(middle, 260);
        mygamesbtn.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        mygamesbtn.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToMyGames();
            }
        });

        TextButton instruction = new TextButton("Instructions", skin);
        instruction.setPosition(middle, 120);
        instruction.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        instruction.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToInstructions();
            }
        });


        stage.addActor(playbtn);
        stage.addActor(mygamesbtn);
        stage.addActor(settings);
        stage.addActor(instruction);

    }

    /**
     * It takes you to the play screen.
     */
    private void goToPlay() {
        game.setScreen(new PlayScreen(game));
        dispose();
    }

    /**
     * It takes you to the MyScores Screen.
     */
    private void goToMyGames() {
        game.setScreen(new MyScoresScreen(game));
        dispose();
    }

    /**
     * It takes you to the instruction screen.
     */
    private void goToInstructions() {
        game.setScreen((new InstructionScreen(game)));
        dispose();
    }

    /**
     * It takes you to the settings screen.
     */
    private void goToSettings() {
        game.setScreen((new SettingsScreen(game)));
        dispose();
    }


    /**

     * It renders the menu screen.
     * @param delta gives the interval of the calls of the method.
     *              The method draws the text and buttons on the screen.
     */
    @Override
    public void render(float delta) {
        SpriteBatch sb = new SpriteBatch();

        sb.begin();
        sb.draw(background, 0, 0, stage.getWidth(), stage.getHeight());
        font.draw(sb, "Menu",
                (int) (AsteroidUI.WIDTH / 2 - 1.5 * font.getData().capHeight),
                (int) (AsteroidUI.HEIGHT / 1.10));
        sb.end();
        stage.act();
        stage.draw();
    }

}
