package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import database.connection.Score;
import gui.AsteroidUI;

/**
 * This screen will show you the top scores, personal and globally.
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class MyScoresScreen extends BasicScreen implements Screen {

    private transient Stage scoreStage;
    private transient TextButton retbtn;
    private transient TextButton change;
    private transient boolean topOrMy;

    /**
     * The screen displaying games played by the user.
     *
     * @param game The AsteroidUI instance.
     *             It initializes all of the UI parts, as in buttons background and labels.
     */
    public MyScoresScreen(AsteroidUI game) {
        super(game);
        scoreStage = new Stage();

        topOrMy = true;

        makeChange();
        makeReturn();

        stage.addActor(retbtn);
        stage.addActor(change);
        switchScores();
    }

    /**
     * this method creates the return button.
     */
    private void makeReturn() {
        retbtn = new TextButton("Menu", skin);
        retbtn.setPosition(middle, 125);
        retbtn.setSize(300, 60);
        retbtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                returnToMenu();
            }
        });
    }

    /**
     * This method creates the changeButton.
     */
    private void makeChange() {
        change = new TextButton("See all time high scores", skin);
        change.setPosition(middle, 195);
        change.setSize(300, 60);
        change.addListener((new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                switchScores();
            }
        }));
    }

    /**
     * It renders the leaderboard screen.
     *
     * @param delta gives the interval of the calls of the method.
     *              The method draws the buttons and high scores dependent on which is selected.
     */
    @Override
    public void render(float delta) {
        super.render(delta);
        scoreStage.act();
        scoreStage.draw();

    }

    /**
     * The method disposes the current screen by deleting background and stage.
     */
    @Override
    public void dispose() {
        super.dispose();
        scoreStage.dispose();
    }

    /**
     * This method checks which scores list needs to be represented.
     */
    private void switchScores() {
        if (topOrMy) {
            showMyScores();
        } else {
            showHighScores();
        }
    }

    /**
     * This makes a list of the users high scores and change the button text.
     */
    private void showMyScores() {
        scoreStage.clear();
        float y = 475;

        Label scores = new Label("See personal high scores", skin);
        scores.setColor(255, 204, 255, 1);
        scores.setPosition(middle, y + 15);
        scores.setFontScale(2);
        scoreStage.addActor(scores);
        for (int i = 0; i < game.getUser().getScores().size() && i < 10; i++) {
            Score s = game.getUser().getScores().get(i);
            Label score = new Label(i + 1 + ": "
                    + s.getNickname() + " Score: " + s.getPoints(), skin);
            score.setColor(0, 1, 0, 1);
            score.setPosition(middle, y -= 20);
            scoreStage.addActor(score);
        }
        topOrMy = false;
        change.setText("All time high scores");
    }


    /**
     * This makes a list of the globally achieved high scores.
     */
    private void showHighScores() {
        scoreStage.clear();
        float y = 475;
        int i = 1;
        Label scores = new Label("Global Leaderboard", skin);
        scores.setColor(255, 204, 255, 1);
        scores.setPosition(middle, y + 15);
        scores.setFontScale(2);
        scoreStage.addActor(scores);
        for (Score s : game.getController().getTopScores()) {
            Label score = new Label(i + ": " + s.getNickname() + " Score: " + s.getPoints(), skin);
            score.setColor(0, 1, 0, 1);
            score.setPosition(middle, y -= 20);
            scoreStage.addActor(score);
            i++;
        }
        topOrMy = true;
        change.setText("My high scores");
    }


    /**
     * This takes you to the main menu.
     */
    private void returnToMenu() {
        game.setScreen(new MenuScreen(game));
        dispose();
    }
}
