package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import database.connection.Score;
import game.Game;
import gui.AsteroidUI;

/**
 * This class handles the screen visible when you die in game.
 */
public class DeathScreen extends BasicScreen implements Screen {

    private transient TextField nickName;
    private transient TextButton submit;
    private transient TextButton menu;
    private transient TextButton retry;
    private transient Label error;
    private transient Label success;
    private transient int score;

    /**
     * The Death screen in the app.
     *
     * @param game This screen shows up if you die and will allow you to submit your score.
     *             It initializes all of the UI parts as in buttons background and error messages.
     */
    public DeathScreen(AsteroidUI game, Game gameScreen) {
        super(game);
        score = gameScreen.getScore();

        Skin skin = new Skin(Gdx.files.internal("skin/neon-ui.json"));
        nickName = new TextField("", skin);
        nickName.setPosition(middle, 330);
        nickName.setSize(300, 60);
        nickName.setMessageText("nickname");

        success = new Label("Your submission was successful", skin);
        error = new Label("Your submission was unsuccessful,\nplease try again", skin);

        makeLabel(success);
        makeLabel(error);
        makeRetry();
        makeMenu();
        makeSubmit();

        stage.addActor(menu);
        stage.addActor(retry);
        stage.addActor(submit);
        stage.addActor(nickName);
        stage.addActor(error);
        stage.addActor(success);
    }

    /**
     * This will take you to the main menu.
     */
    public void menuAction() {
        game.setScreen(new MenuScreen(game));
        dispose();
    }

    /**
     * Called to make success button.
     */
    private void makeLabel(Label label) {
        label.setSize(300, 40);
        label.setPosition(stage.getWidth() / 2 - 150, 50);
        label.setAlignment(Align.center);
        label.setVisible(false);
    }


    /**
     * Called to make retry button.
     */
    private void makeRetry() {
        retry = new TextButton("Retry", skin);
        retry.setPosition(stage.getWidth() / 2 - 150, 190);
        retry.setSize(300, 60);
        retry.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                retryAction();
            }
        });
    }

    /**
     * Called to make menu button.
     */
    private void makeMenu() {
        menu = new TextButton("Menu", skin);
        menu.setPosition(stage.getWidth() / 2 - 150, 120);
        menu.setSize(300, 60);
        menu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                menuAction();
            }
        });
    }

    /**
     * Called to make submit button.
     */
    private void makeSubmit() {
        submit = new TextButton("Submit score", skin);
        submit.setPosition(stage.getWidth() / 2 - 150, 260);
        submit.setSize(300, 60);
        submit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                submitAction();
            }
        });

    }

    /**
     * This will take you to a new playscreen.
     */
    public void retryAction() {
        game.setScreen(new PlayScreen(game));
        dispose();
    }

    /**
     * It gets the text from the field and tries to save your score in the database.
     * If this goes correct it will show a message to confirm and otherwise it does give an error.
     */
    public void submitAction() {
        String name = nickName.getText();
        if (game.getController().saveScores(game.getUser(), name, new Score(score))) {
            error.setVisible(false);
            success.setVisible(true);


        } else {
            success.setVisible(false);
            error.setVisible(true);
        }
    }

    /**
     * It renders the death screen.
     *
     * @param delta gives the interval of the calls of the method.
     *              The method draws the score and buttons on the screen.
     */
    @Override
    public void render(float delta) {
        SpriteBatch sb = new SpriteBatch();
        Gdx.gl.glClearColor(30, 90, 200, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0, 0, stage.getWidth(), stage.getHeight());
        font.draw(sb, "Game Over\nScore: " + score,
                (int) (AsteroidUI.WIDTH / 2.5 - 1.5 * font.getData().capHeight),
                (int) (AsteroidUI.HEIGHT / 1.10));

        sb.end();
        stage.act();
        stage.draw();
    }

}


