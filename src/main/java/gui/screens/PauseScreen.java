package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import database.connection.Score;
import game.Game;
import gui.AsteroidUI;
import gui.Controller;

/**
 * This screen functions as a pause screen when you want to take a moment during the game.
 */
public class PauseScreen extends BasicScreen implements Screen {

    private transient Game gameScreen;


    /**
     * The pause screen of the game.
     *
     * @param game   The game GUI.
     * @param screen The current game Screen.
     *               It initializes all of the UI parts, as in buttons background and text.
     */
    public PauseScreen(AsteroidUI game, Game screen) {
        super(game);
        gameScreen = screen;

        TextButton resumebtn = new TextButton("Play", skin);
        resumebtn.setPosition(middle, 260);
        resumebtn.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        resumebtn.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToPlay();
            }
        });

        TextButton quitbtn = new TextButton("Quit", skin);
        quitbtn.setPosition(middle, 190);
        quitbtn.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        quitbtn.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                quitGame();
            }
        });


        TextButton settings = new TextButton("Settings", skin);
        settings.setPosition(middle, 120);
        settings.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        settings.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToSettings();
            }
        });

        stage.addActor(resumebtn);
        stage.addActor(quitbtn);
        stage.addActor(settings);
    }

    /**
     * It takes you to the play screen.
     */
    private void goToPlay() {
        game.setScreen(new PlayScreen(game, gameScreen));
        dispose();
    }

    /**
     * Saves score and returns to menu screen.
     */
    private void quitGame() {
        game.setScreen(new DeathScreen(game, gameScreen));
        dispose();
    }

    /**
     * It takes you to the settings screen.
     */
    private void goToSettings() {
        game.setScreen(new SettingsScreen(game, gameScreen));
        dispose();
    }


}

