package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import database.connection.User;
import gui.AsteroidUI;
import gui.Controller;

/**
 * The screen where a user can register.
 */
public class RegisterScreen extends BasicScreen implements Screen {
    private transient Controller controller;
    private transient TextButton signup;
    private transient TextButton signin;
    private transient TextField password;
    private transient TextField email;
    private transient Label error;
    private transient float middle;

    /**
     * Register screen of the game.
     * @param game the gama GUI.
     */
    protected RegisterScreen(AsteroidUI game) {
        super(game);
        controller = new Controller();

        middle = stage.getWidth() / 2 - 150;

        makeSignUp();
        makeSignIn();
        makeEmail();
        makePassword();
        makeError();

        stage.addActor(error);
        stage.addActor(password);
        stage.addActor(email);
        stage.addActor(signup);
        stage.addActor(signin);
    }

    /**
     * This makes the error.
     */
    private void makeError() {
        error = new Label("", skin);
        error.setSize(300, 40);
        error.setPosition(middle, 120);
        error.setAlignment(Align.center);
        error.setVisible(false);
    }

    /**
     * This makes the password field.
     */
    private void makePassword() {
        password = new TextField("", skin);
        password.setPosition(middle, 310);
        password.setSize(300, 60);
        password.setPasswordCharacter('*');
        password.setPasswordMode(true);
        password.setMessageText("password");
    }

    /**
     * This makes the email field.
     */
    private void makeEmail() {
        email = new TextField("", skin);
        email.setPosition(middle, 370);
        email.setSize(300, 60);
        email.setMessageText("email");
    }

    /**
     * This makes the signin button.
     */
    private void makeSignIn() {
        signin = new TextButton("Sign In", skin);
        signin.setPosition(middle, 180);
        signin.setSize(300, 60);
        signin.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                signInAction();
            }
        });
    }

    /**
     * This makes the sign up button.
     */
    private void makeSignUp() {
        signup = new TextButton("Sign Up", skin);
        signup.setPosition(middle, 250);
        signup.setSize(300, 60);
        signup.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                signupAction();
            }
        });
    }

    /**
     * Signs up the user based on input.
     */
    private void signupAction() {

        if (email.getText().equals("")) {
            error.setText("Please enter an email.");
            error.setVisible(true);
            return;
        }
        if (password.getText().equals("")) {
            error.setText("Please enter a password");
            error.setVisible(true);
            return;
        }
        User user = new User(email.getText(), password.getText());
        if (!controller.registerUser(user)) {
            error.setText("There already exists a account with this email,"
                    + " \n please try another address.");
            error.setVisible(true);
            return;
        }

        game.setScreen(new LoginScreen(game));
        dispose();

    }

    /**
     * Changes screen to login screen.
     */
    private void signInAction() {
        game.setScreen(new LoginScreen(game));
        dispose();
    }

    /**
     * Renders the elements on the screen.
     * @param delta the delta time of the game.
     */
    @Override
    public void render(float delta) {
        SpriteBatch sb = new SpriteBatch();
        Gdx.gl.glClearColor(30, 90, 200, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0, 0, stage.getWidth(), stage.getHeight());
        font.draw(sb, "Asteroids",
                (int) (AsteroidUI.WIDTH / 2 - 3 * font.getData().capHeight),
                (int) (AsteroidUI.HEIGHT / 1.15));

        sb.end();
        stage.act();
        stage.draw();
    }
}
