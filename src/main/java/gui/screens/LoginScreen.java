package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import database.connection.User;
import gui.AsteroidUI;
import gui.Controller;

/**
 * This screen screen is used to login into the game.
 */
public class LoginScreen extends BasicScreen implements Screen, InputProcessor {
    private transient Controller controller;
    private transient TextButton loginButton;
    private transient TextButton registrationButton;
    private transient TextField password;
    private transient TextField email;
    private transient Label error;


    /**
     * The login screen of the game.
     *
     * @param game The game GUI
     *             It initializes all of the UI parts as in buttons background and error messages.
     */
    public LoginScreen(AsteroidUI game) {
        super(game);
        controller = game.getController();
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(this);
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);
        makeloginButton();
        makeregistrationButton();
        emailButton(middle);
        passwordButton(middle);


        errorMessage(middle);

        error.setVisible(false);

        stage.addActor(error);
        stage.addActor(email);
        stage.addActor(password);
        stage.addActor(registrationButton);
        stage.addActor(loginButton);
        game.musicStart();

    }

    /**
     * Called to make error message.
     */
    private void errorMessage(float middle) {
        error = new Label("", skin);
        error.setSize(300, 40);
        error.setPosition(middle, 120);
        error.setAlignment(Align.center);
    }

    /**
     * Called to make password button.
     */
    private void passwordButton(float middle) {
        password = new TextField("", skin);
        password.setPosition(middle, 330);
        password.setSize(300, 60);
        password.setPasswordCharacter('*');
        password.setPasswordMode(true);
        password.setMessageText("password");
    }

    /**
     * Called to make email button.
     */
    private void emailButton(float middle) {
        email = new TextField("", skin);
        email.setPosition(middle, 400);
        email.setSize(300, 60);
        email.setMessageText("email");
    }

    /**
     * Called to make login button.
     */
    private void makeloginButton() {
        float middle = stage.getWidth() / 2 - 150;
        loginButton = new TextButton("Login", skin);
        loginButton.setPosition(middle, 260);
        loginButton.setSize(300, 60);
        loginButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                loginAction();
            }
        });

    }

    /**
     * Called to make registration button.
     */
    private void makeregistrationButton() {
        registrationButton = new TextButton("Register", skin);
        registrationButton.setPosition(middle, 190);
        registrationButton.setSize(300, 60);
        registrationButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                registerAction();
            }
        });
    }


    /**
     * Renders the login screen.
     *
     * @param delta gives the interval of the calls of the method
     *              The method draws the text and buttons on the screen.
     */
    @Override
    public void render(float delta) {
        SpriteBatch sb = new SpriteBatch();

        Gdx.gl.glClearColor(30, 90, 200, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0, 0, stage.getWidth(), stage.getHeight());
        font.draw(sb, "Asteroids",
                (int) (AsteroidUI.WIDTH / 2 - 3 * font.getData().capHeight),
                (int) (AsteroidUI.HEIGHT / 1.10));
        sb.end();
        stage.act();
        stage.draw();
    }


    /**
     * Connects to the DB to check if credentials correct.
     * If correct -> sets state to Menu state.
     */
    private void loginAction() {

        if (email.getText().equals("")) {
            error.setText("Please fill in the email field.");
            error.setVisible(true);
            return;
        }
        if (password.getText().equals("")) {
            error.setText("Please enter your password.");
            error.setVisible(true);
            return;
        }
        User user = new User(email.getText(), password.getText());
        if (!controller.userLogin(user)) {
            error.setText("Your login credentials are incorrect, \nplease try again.");
            error.setVisible(true);
        } else {
            game.setUser(user);
            game.setScreen(new MenuScreen(game));
            dispose();
        }
    }

    /**
     * registers the action.
     */
    private void registerAction() {

        game.setScreen(new RegisterScreen(game));
        dispose();
    }


    /**
     * Called when a key was pressed.
     *
     * @param keycode one of the constants in {@link Input.Keys}
     * @return whether the input was processed
     */
    @Override
    public boolean keyDown(int keycode) {
        if (Input.Keys.ENTER == keycode) {
            loginAction();
        }
        return false;
    }

    /**
     * Called when a key was released.
     *
     * @param keycode one of the constants in {@link Input.Keys}
     * @return whether the input was processed
     */
    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    /**
     * Called when a key was typed.
     *
     * @param character The character
     * @return whether the input was processed
     */
    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    /**
     * Called when the screen was touched or a mouse button was pressed.
     * The button parameter will be {@link Input.Buttons#LEFT} on iOS.
     *
     * @param screenX The x coordinate, origin is in the upper left corner
     * @param screenY The y coordinate, origin is in the upper left corner
     * @param pointer the pointer for the event.
     * @param button  the button
     * @return whether the input was processed
     */
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    /**
     * Called when a finger was lifted or a mouse button was released.
     * The button parameter will be {@link Input.Buttons#LEFT} on iOS.
     *
     * @param screenX the x coordinate of the touch
     * @param screenY the y coordinate of the touch
     * @param pointer the pointer for the event.
     * @param button  the button
     * @return whether the input was processed
     */
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    /**
     * Called when a finger or the mouse was dragged.
     *
     * @param screenX the x coordinate of the touch
     * @param screenY the y coordinate of the touch
     * @param pointer the pointer for the event.
     * @return whether the input was processed
     */
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    /**
     * Called when the mouse was moved without any buttons being pressed. Will not be called on iOS.
     *
     * @param screenX the x coordinate of the touch
     * @param screenY the y coordinate of the touch
     * @return whether the input was processed
     */
    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    /**
     * Called when the mouse wheel was scrolled. Will not be called on iOS.
     *
     * @param amount the scroll amount, -1 or 1 depending on the direction the wheel was scrolled.
     * @return whether the input was processed.
     */
    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
