package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.Game;
import gui.AsteroidUI;

/**
 * The screen where a user can change their settings.
 */
public class SettingsScreen extends BasicScreen implements Screen {

    private transient TextButton pausebtn;
    private transient Game gameScreen;
    private transient TextButton retbtn;

    /**
     * The settings screen in the app.
     * @param game The UI Game instance.
     */
    public SettingsScreen(AsteroidUI game) {
        super(game);
        initButtons();
        float middle = stage.getWidth() / 2 - 150;
        makeReturnMenu(middle);

    }

    /**
     * Second constructor for when called from game pausing.
     * @param game the UI game.
     * @param gameScreen The current game screen.
     */
    public SettingsScreen(AsteroidUI game, Game gameScreen) {
        super(game);

        this.gameScreen = gameScreen;
        initButtons();

        font = skin.getFont("font-over");
        font.getData().scale((float) 1.5);

        makeReturnPause(middle);
        stage.addActor(retbtn);
        background = new Texture("skybg.jpg");
    }

    /**
     * Makes the return button.
     * @param middle Position on the screen
     */
    private void makeReturnPause(float middle) {
        retbtn = new TextButton("Menu", skin);
        retbtn.setPosition(middle, 190);
        retbtn.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        retbtn.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToPause();
            }
        });
        stage.addActor(retbtn);
    }

    /**
     * Makes the return button.
     * @param middle Position on the screen
     */
    private void makeReturnMenu(float middle) {
        retbtn = new TextButton("Return", skin);
        retbtn.setPosition(middle, 190);
        retbtn.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        retbtn.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent e, float x, float y, int point, int button) {
                goToMenu();
            }
        });
        stage.addActor(retbtn);
    }

    private void initButtons() {
        float middle = stage.getWidth() / 2 - 150;
        pausebtn = new TextButton("Toggle Music", skin);
        pausebtn.setPosition(middle, 260);
        pausebtn.setSize(AsteroidUI.STD_BTN_WIDTH, AsteroidUI.STD_BTN_HEIGHT);
        pausebtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                toggleMusic();
            }
        });
        stage.addActor(pausebtn);
    }

    /**
     * Renders the elements on the screen.
     * @param delta the delta time of the game.
     */
    @Override
    public void render(float delta) {
        if (game.musicPlaying) {
            pausebtn.setText("Toggle music off");
        } else {
            pausebtn.setText("Toggle music on");
        }
        SpriteBatch sb = new SpriteBatch();
        Gdx.gl.glClearColor(30, 90, 200, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0, 0, stage.getWidth(), stage.getHeight());
        font.draw(sb, "Settings",
                        (int) (AsteroidUI.WIDTH / 2 - 3 * font.getData().capHeight),
                        (int) (AsteroidUI.HEIGHT / 1.10));
        sb.end();
        stage.act();
        stage.draw();
    }

    /**
     * Toggles music on/off.
     */
    private void toggleMusic() {
        if (game.musicPlaying) {
            game.musicEnd();
        } else {
            game.musicStart();
        }
    }

    /**
     * Changes screen to menu.
     */
    private void goToMenu() {
        game.setScreen(new MenuScreen(game));
        dispose();
    }

    /**
     * Changes screen to pause.
     */
    private void goToPause() {
        game.setScreen(new PauseScreen(game, gameScreen));
        dispose();
    }
}
