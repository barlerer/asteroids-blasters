package gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import gui.AsteroidUI;

/**
 * The basic screen superclass.
 */
public class BasicScreen implements Screen {
    protected transient AsteroidUI game;
    protected transient Texture background;
    protected transient BitmapFont font;
    protected transient Stage stage;
    protected transient Skin skin;
    protected transient float middle;


    /**
     * The Basic Screen superclass.
     * @param game the GUI game.
     */
    public BasicScreen(AsteroidUI game) {
        this.game = game;

        stage = new Stage();
        middle = stage.getWidth() / 2 - 150;

        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/neon-ui.json"));
        font = skin.getFont("font-over");
        font.getData().scale((float) 1.5);
        background = new Texture("skybg.jpg");


    }

    /**
     * Shows.
     */
    @Override
    public void show() {

    }

    /**
     * Basic rendering method.
     * @param delta the delta time.
     */
    @Override
    public void render(float delta) {
        SpriteBatch sb = new SpriteBatch();
        Gdx.gl.glClearColor(30, 90, 200, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0, 0, stage.getWidth(), stage.getHeight());
        sb.end();
        stage.act();
        stage.draw();
    }

    /**
     * Inherits from screen.
     */
    @Override
    public void resize(int width, int height) {

    }

    /**
     * Inherits from screen.
     */
    @Override
    public void pause() {

    }

    /**
     * Inherits from screen.
     */
    @Override
    public void resume() {

    }

    /**
     * Inherits from screen.
     */
    @Override
    public void hide() {

    }

    /**
     * The method disposes the current screen by deleting background and stage.
     */
    @Override
    public void dispose() {
        background.dispose();
        stage.dispose();
    }
}
