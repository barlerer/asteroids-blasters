package gui;

import database.connection.Score;
import database.connection.User;

import java.util.List;

public interface ControllerInterface {
    boolean registerUser(User user);

    List<Score> getTopScores();
}
