package gui;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * The class is used to start the game following the LibGDX procedure.
 */
public class StartGui {

    /**
     * This main class starts the window for the game and menu.
     *
     * @author SEM Group 54
     * @version 1.0
     * @since 1.0
     */
    public static void main(String[] arg) {
        ConfigLoader.readValues("config.properties");
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = AsteroidUI.TITLE;
        config.width = AsteroidUI.WIDTH;
        config.height = AsteroidUI.HEIGHT;

        new LwjglApplication(new AsteroidUI(), config);
    }
}

