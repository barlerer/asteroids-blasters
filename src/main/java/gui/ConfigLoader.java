package gui;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Reads all configuration properties.
 * @author Bar Lerer
 * @author SEM-54
 * @version 1.0
 * @since 1.0
 */
public class ConfigLoader {

    private static String dBUser;
    private static String dBPassword;
    private static String dBUrl;
    private static String testDBUser;
    private static String testDBPassword;
    private static String testDBUrl;
    private static boolean isReadValues;

    /**
     * Getter for the test database user.
     * @return the test database user
     */
    public static String getTestDbUser() {
        return testDBUser;
    }

    /**
     * Getter for the test database password.
     * @return The test database password
     */
    public static String getTestDbPassword() {
        return testDBPassword;
    }

    /**
     * Getter for test database url.
     * @return the test database url
     */
    public static String getTestDbUrl() {
        return testDBUrl;
    }

    /**
     * Getter for real database user.
     * @return real database user
     */
    public static String getdBUser() {
        return dBUser;
    }

    /**
     * Getter for real database password.
     * @return real database password
     */
    public static String getdBPassword() {
        return dBPassword;
    }

    /**
     * Getter for real database url.
     * @return real database url
     */
    public static String getdBUrl() {
        return dBUrl;
    }

    /**
     * Checks if the values are already read from the file.
     * @return true if config file is already read
     */
    public static boolean getterIsReadValues() {
        return isReadValues;
    }

    /**
     * Loads the config file from the specified file.
     * @param fileName The file to load configurations from
     */
    public static void readValues(String fileName) {
        Properties properties = new Properties();
        InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(fileName);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cannot find file");
        }
        isReadValues = true;
        dBUser = properties.getProperty("DBUser");
        dBPassword = properties.getProperty("DBPassword");
        dBUrl = properties.getProperty("DBUrl");
        testDBUrl = properties.getProperty("TestDBUrl");
        testDBPassword = properties.getProperty("TestDBPassword");
        testDBUser = properties.getProperty("TestDBUser");
    }
}
