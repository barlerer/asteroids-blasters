package gui;

import database.connection.DatabaseAccess;
import database.connection.Score;
import database.connection.User;
import java.sql.Connection;
import java.util.List;

import org.apache.commons.validator.routines.EmailValidator;

/**
 * Controller to be used by the system, handles all interaction with database.
 * @author Bar Lerer
 * @author SEM-54
 * @version 1.0
 * @since 1.0
 */
public class Controller implements ControllerInterface {
    private transient DatabaseAccess databaseAccess;
    private transient Connection connection;
    private transient String dbUrl;
    private transient String dbUser;
    private transient String dbPassword;
    private ConfigLoader configLoader;

    /**
     * Constructor for controller class.
     * @param dbUrl URL of database
     * @param user  User of database
     * @param dbPassword Password of database
     */
    public Controller(String dbUrl, String user, String dbPassword) {
        databaseAccess = new DatabaseAccess();
        this.dbUrl = dbUrl;
        this.dbUser = user;
        this.dbPassword = dbPassword;
        connection = databaseAccess.connectToDB(dbUrl, dbUser, dbPassword);
    }

    /**
     * Getter for the connection to the DB.
     * @return
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Empty constructor that uses default values.
     */
    public Controller() {
        databaseAccess = new DatabaseAccess();
        this.dbUrl = ConfigLoader.getdBUrl();
        this.dbUser = ConfigLoader.getdBUser();
        this.dbPassword = ConfigLoader.getdBPassword();
        connection = databaseAccess.connectToDB(dbUrl, dbUser, dbPassword);
    }

    /**
     * This methods will register a user.
     * If there is no connection to the database or email is already
     * in the system, registration will fail.
     * @param user The user of which we are going to register
     * @return true if user was registered.
     */
    @Override
    public boolean registerUser(User user) {
        if (EmailValidator.getInstance().isValid(user.getEmail())) {
            user.cryptPassword();
            return databaseAccess.registerUser(user);
        } else {
            return false;
        }
    }

    /**
     * Gets the top scores in the database.
     * I have set the number of top scores to 10, but it is
     * adjustable.
     * @return An list with top user scores, and their nicknames.
     */
    @Override
    public List<Score> getTopScores() {
        List<Score> scores = databaseAccess.getTopScores();
        return scores;
    }

    /**
     * Allows for user log-on.
     * @param user The user that want to log in
     * @return True if user successfully logged on
     */
    public boolean userLogin(User user) {
        if (EmailValidator.getInstance().isValid(user.getEmail())) {
            return databaseAccess.userLogin(user);
        } else {
            return false;
        }
    }

    /**
     * Updates the score of a user according to whats in the database.
     * @param user The user of whom we want to get its scores.
     */
    public void getScores(User user) {
        databaseAccess.getScores(user);
    }

    /**
     * Saves the user new score.
     * @param user The user of which we want to save the score
     * @param nickname The nickname the use choose
     * @param score The score we want to save
     * @return True if save successfully happened
     */
    public boolean saveScores(User user, String nickname, Score score) {
        if (nickname == null || nickname.isEmpty() || score == null) {
            return false;
        }
        return databaseAccess.saveScore(user, nickname, score);
    }


}
