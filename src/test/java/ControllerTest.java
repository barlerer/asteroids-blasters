
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import database.connection.Hashing;
import database.connection.User;
import gui.ConfigLoader;
import gui.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ControllerTest {

    private static final String userEmail = "test@gmail.com";
    private static final String userPassword = "123456";
    private static String user;
    private static String password;
    private static String url;
    private static Controller controller;
    private static Connection connection;
    private static Hashing hashing;
    private static User badUser;

    /**
     * Setup for all tests, connects to
     * testing database.
     */
    @BeforeAll
    public void setUp() {
        ConfigLoader.readValues("config.properties");
        hashing = new Hashing();
        user = ConfigLoader.getTestDbUser();
        password = ConfigLoader.getTestDbPassword();
        url = ConfigLoader.getTestDbUrl();
        controller = new Controller(url, user, password);
        connection = controller.getConnection();
        badUser = new User("Testtest.com", userPassword);
    }

    /**
     * For clean testing platform, should create the schema
     * for every test and put the demo data in.
     */
    @BeforeEach
    public void creatingTable() {
        try {
            connection.createStatement().execute("\n"
                    + "CREATE TABLE `users` (\n"
                    + "\t`id` int NOT NULL AUTO_INCREMENT,\n"
                    + "\t`email` varchar(200) NOT NULL UNIQUE,\n"
                    + "\t`password` varchar(100) NOT NULL,\n"
                    + "\t`registration_date` TIMESTAMP DEFAULT "
                    + "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n"
                    + "\tPRIMARY KEY (`id`)\n);");
            connection.createStatement().execute("CREATE TABLE `scores` (\n"
                    + "\t`score_id` int NOT NULL AUTO_INCREMENT,\n"
                    + "\t`user_id` int NOT NULL,\n"
                    + "\t`score` int NOT NULL,\n"
                    + "\t`nickname` varchar(100) NOT NULL DEFAULT 'Unknown',\n"
                    + "\tPRIMARY KEY (`score_id`)\n);");
            connection.createStatement().execute("ALTER TABLE `scores` ADD CONSTRAINT `scores_fk0` "
                    + "FOREIGN KEY (`user_id`) REFERENCES `users`(`id`);");
            PreparedStatement userStatment = connection
                    .prepareStatement("INSERT INTO users (email, password) "
                            + "VALUES (?, ?);");
            userStatment.setString(1, userEmail);
            userStatment.setString(2, hashing.crypt(userPassword));
            userStatment.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Dropping all tables after every test.
     */
    @AfterEach
    public void tearDown() {
        try {
            connection.createStatement().execute("DROP TABLE IF EXISTS scores;");
            connection.createStatement().execute("DROP TABLE IF EXISTS users;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    public void tearDownAll() throws SQLException {
        connection.close();
    }


    @Test
    public void testRegister() {
        User user = new User("willthis@works.com", userPassword);
        assertTrue(controller.registerUser(user));
    }

    @Test
    public void testBadRegister() {
        User user = new User(userEmail, userPassword);
        assertFalse(controller.registerUser(user));
    }

    @Test
    public void testLoginBadEmail() {
        User user = new User("test@gmail123.com", userPassword);
        assertFalse(controller.userLogin(user));
    }

    @Test
    public void testLoginBadPassword() {
        User user = new User(userEmail, "1234567");
        assertFalse(controller.userLogin(user));
    }

    @Test
    public void testLoginGood() {
        User user = new User("test@gmail.com", userPassword);
        assertTrue(controller.userLogin(user));
    }

    @Test
    public void testDropTables() {
        User testDrop = new User(";DROP TABLE users", userPassword);
        User user2 = new User("Test@test.com", userPassword);
        controller.registerUser(testDrop);
        assertTrue(controller.registerUser(user2));
    }

    @Test
    public void connectToDefaultDB() {
        Controller controllerDB = new Controller();
        try {
            assertTrue(controllerDB.getConnection().isValid(60));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void emailValidationBadEmail() {
        assertFalse(controller.registerUser(badUser));
    }

    @Test
    public void emailValidationBadEmail2() {
        User noEndingUser = new User("aaa@gmail", userPassword);
        assertFalse(controller.registerUser(noEndingUser));
    }

    @Test
    public void emailValidationBadEmailLogin() {
        assertFalse(controller.userLogin(badUser));
    }

    @Test
    public void emailValidationBadEmail2Login() {
        User noEndingUser = new User("aaa@gmail", userPassword);
        assertFalse(controller.userLogin(noEndingUser));
    }


}
