package game.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.initMocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.badlogic.gdx.math.Polygon;

import com.badlogic.gdx.math.Rectangle;
import game.Asteroid;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class AsteroidTest {
    private transient Asteroid asteroid1;
    private transient Asteroid asteroid2;

    @Mock
    private transient TextureRegion textureRegion;

    @BeforeEach
    void setUp() {
        initMocks(this);
        asteroid1 = new Asteroid(textureRegion);
        asteroid2 = mock(Asteroid.class);
    }

    @Test
    void asteroidVelocityTest() {
        asteroid1.setVelX(10);
        asteroid1.setVelY(20);
        assertEquals(asteroid1.getVelX(), 10);
        assertEquals(asteroid1.getVelY(), 20);
    }

    @Test
    void asteroidLocationTest() {
        asteroid1.setX(44);
        asteroid1.setY(44);
        assertEquals(asteroid1.getY(), 44);
        assertEquals(asteroid1.getxValue(), 44);
        asteroid1.incX(4);
        asteroid1.incY(4);
        assertEquals(asteroid1.getY(), 48);
        assertEquals(asteroid1.getxValue(), 48);
    }

    @Test
    void asteroidAngleTest() {
        asteroid1.setFaceAngle(20);
        asteroid1.setMoveAngle(40);
        assertEquals(asteroid1.getMoveAngle(), 40);
        assertEquals(asteroid1.getFaceAngle(), 20);
        asteroid1.incFaceAngle(5);
        asteroid1.incMoveAngle(5);
        assertEquals(asteroid1.getMoveAngle(), 45);
        assertEquals(asteroid1.getFaceAngle(), 25);

    }

    @Test
    void asteroidOriginTest() {
        asteroid1.setRotationVelocity(20);
        assertEquals(asteroid1.getRotationVelocity(), 20);
    }

    @Test
    void asteroidShapeTest() {
        float[] shape = {-6,7, -8,7, 9,7, 6,1, 3,3, 9,-7};
        asteroid1.setShape(new Polygon(shape));
        assertEquals(asteroid1.getShape().getVertices(), shape);
    }

    @Test
    void asteroidAliveTest() {
        assertTrue(asteroid1.isAlive());
        asteroid1.setAlive(false);
        assertFalse(asteroid1.isAlive());
    }

    @Test
    void asteroidBoundsTest() {
        Rectangle bounds = new Rectangle(
            (int) asteroid1.getxValue() - (30 * asteroid1.getSplitCount()),
            (int) asteroid1.getY() - (30 * asteroid1.getSplitCount()),
            (60 * asteroid1.getSplitCount()), (60 * asteroid1.getSplitCount()));

        assertEquals(bounds, asteroid1.getBounds());
    }

    @Test
    void textureTest() {
        Mockito.when(asteroid2.getAsteroidRegion()).thenReturn(textureRegion);
        assertEquals(textureRegion, asteroid2.getAsteroidRegion());
    }

    @Test
    void testGetter() {
        assertTrue(asteroid1.getAsteroidRegion().equals(textureRegion));
    }

    @Test
    void testSetter() {
        TextureRegion newTexture = mock(TextureRegion.class);
        asteroid1.setAsteroidRegion(newTexture);
        assertTrue(asteroid1.getAsteroidRegion().equals(newTexture));
        assertFalse(asteroid1.getAsteroidRegion().equals(textureRegion));
    }

    @Test
    void testDecSplitCount() {
        asteroid1.decSplitCount();
        assertEquals(1, asteroid1.getSplitCount());
    }
}
