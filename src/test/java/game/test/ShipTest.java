package game.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import game.Bullet;
import game.Ship;
import gui.screens.PlayScreen;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;



public class ShipTest {

    private transient Ship ship;
    private transient Ship ship2;
    private transient Bullet bullet;
    private transient TextureRegion textureRegion;
    private transient Bullet bullet2;

    private void keyDownTestHelper() {
        ship.setMoveAngle(0);
        ship.setFaceAngle(100);
        ship.setVelX(30);
        ship.setVelY(30);
    }

    @BeforeEach
    void setUp() {
        ship = new Ship(textureRegion);
        bullet = new Bullet(ship.getVelX(), ship.getVelY(), textureRegion);
        ship2 = mock(Ship.class);
        bullet2 = new Bullet(4, 5, textureRegion);
    }

    @Test
    void shipVelocityTest() {
        ship.setVelX(10);
        ship.setVelY(20);
        assertEquals(ship.getVelX(), 10);
        assertEquals(ship.getVelY(), 20);
    }

    @Test
    void shipLocationTest() {
        ship.setX(44);
        ship.setY(44);
        assertEquals(ship.getY(), 44);
        assertEquals(ship.getxValue(), 44);
        ship.incX(4);
        ship.incY(4);
        assertEquals(ship.getY(), 48);
        assertEquals(ship.getxValue(), 48);
    }

    @Test
    void shipAngleTest() {
        ship.setFaceAngle(20);
        ship.setMoveAngle(40);
        assertEquals(ship.getMoveAngle(), 40);
        assertEquals(ship.getFaceAngle(), 20);
        ship.incFaceAngle(5);
        ship.incMoveAngle(5);
        assertEquals(ship.getMoveAngle(), 45);
        assertEquals(ship.getFaceAngle(), 25);

    }

    @Test
    void shipOriginTest() {
        assertEquals(ship.getOriginX(), 25);
        assertEquals(ship.getOriginY(), 25);
    }

    @Test
    void shipShapeTest() {
        float[] shape = {-6, 7, -8, 7, 9, 7, 6, 1, 3, 3, 9, -7};
        ship.setShape(new Polygon(shape));
        assertEquals(ship.getShape().getVertices(), shape);
    }

    @Test
    void shipBoundsTest() {
        Rectangle r = new Rectangle((int) ship.getxValue() - 6, (int) ship.getY() - 6,
                PlayScreen.SHIPSIZE, PlayScreen.SHIPSIZE);
        assertEquals(ship.getBounds(), r);
    }

    @Test
    void shipRotationTest() {
        ship.setRotation(200);
        assertEquals(ship.getRotation(), 200);
    }

    @Test
    void shipAliveTest() {

        ship.setAlive(false);
        assertFalse(ship.isAlive());
    }

    @Test
    void getBulletsTest() {
        Bullet b = new Bullet(3, 4, textureRegion);
        ArrayList<Bullet> bullets = new ArrayList<>();
        bullets.add(b);

        ship.getBullets().add(b);
        assertEquals(bullets, ship.getBullets());
    }

    @Test
    void textureTest() {
        Mockito.when(ship2.getShip()).thenReturn(textureRegion);
        assertEquals(textureRegion, ship2.getShip());
    }

    @Test
    void fireTest() {
        ship.fire(bullet);
        ArrayList<Bullet> bulletArray = new ArrayList<>();
        bulletArray.add(bullet);
        assertEquals(ship.getBullets(), bulletArray);
    }

    @Test
    void restOfKeysTest() {
        assertFalse(ship.keyUp(0));
        assertFalse(ship.keyTyped('0'));
        assertFalse(ship.touchDown(0, 0, 0, 0));
        assertFalse(ship.touchDragged(0, 0, 0));
        assertFalse(ship.mouseMoved(0, 0));
        assertFalse(ship.scrolled(0));
        assertFalse(ship.touchUp(0, 0, 0, 0));
    }

    @Test
    void keyDownTest1() {
        ship.setFaceAngle(360);
        ship.keyDown(Input.Keys.LEFT);
        assertEquals(10, ship.getFaceAngle());
    }

    @Test
    void keyDownTest2() {
        ship.setFaceAngle(0);
        ship.keyDown(Input.Keys.RIGHT);
        assertEquals(350, ship.getFaceAngle());
    }

    @Test
    void keyDownTest3() {
        keyDownTestHelper();
        double x = ship.getVelX();
        double y = ship.getVelY();
        ship.keyDown(Input.Keys.DOWN);
        double ang = ship.getMoveAngle() * Math.PI / 180;
        assertEquals((x + (Math.cos(ang) * 0.1)), ship.getVelX());
        assertEquals((y + (Math.sin(ang) * 0.1)), ship.getVelY());
        assertEquals(ship.getFaceAngle() - 90, ship.getMoveAngle());
    }

    @Test
    void keyDownTest4() {
        keyDownTestHelper();
        double x = ship.getVelX();
        double y = ship.getVelY();
        ship.keyDown(Input.Keys.UP);
        double ang = ship.getMoveAngle() * Math.PI / 180;
        assertEquals((x - (Math.cos(ang) * 0.1)), ship.getVelX());
        assertEquals((y - (Math.sin(ang) * 0.1)), ship.getVelY());
        assertEquals(ship.getFaceAngle() - 90, ship.getMoveAngle());
    }

    @Test
    void keyDownTest5() {
        assertTrue(ship.keyDown(Input.Keys.U));
    }

}