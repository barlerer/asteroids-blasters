package game.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import game.Asteroid;
import game.Bullet;
import game.Collision;
import game.Game;
import game.Ship;
import gui.AsteroidUI;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GameTest {

    private transient Game game;
    private transient Ship ship;
    private transient Bullet bull;
    private transient Asteroid asteroid;
    private transient ArrayList<Asteroid> as;
    private transient TextureRegion textureRegion;
    private transient Collision collision;


    @BeforeEach
    void setUp() {
        collision = mock(Collision.class);
        textureRegion = mock(TextureRegion.class);
        game = new Game();
        ship = new Ship(textureRegion);
        ship.setX(10);
        ship.setY(10);
        ship.setVelX(10);
        ship.setVelY(10);
        bull = new Bullet(ship.getVelX(), ship.getVelY(), textureRegion);
        asteroid = new Asteroid(textureRegion);
        as = new ArrayList<>();
    }



    @Test
    void scoreInitTest() {
        assertEquals(0, game.getScore());
    }

    @Test
    void scoreTest() {
        game.addScore(10);
        assertEquals(10, game.getScore());
    }


    @Test
    void asteroidListTest() {
        as.add(asteroid);
        game.getAsteroids().add(asteroid);
        assertEquals(as, game.getAsteroids());
    }


    @Test
    void shipUpdate() {
        game.setShip(ship);
        ship.update(2);
        assertEquals((game.getShip().getVelX() * 2) * 10 + 10, game.getShip().getxValue());
        assertEquals((game.getShip().getVelY() * 2) * 10 + 10, game.getShip().getY());
    }

    @Test
    void bulletUpdateOffScreen() {
        bull.setX(900);
        bull.setY(900);
        bull.setVelX(20);
        bull.setVelY(20);
        ArrayList<Bullet> bullets = new ArrayList<>();
        bullets.add(bull);
        ship.setBullets(bullets);
        game.setShip(ship);
        game.bulletUpdate(2);
        assertEquals(0, game.getShip().getBullets().size());
    }

    @Test
    void bulletUpdate() {
        bull.setX(30);
        bull.setY(30);
        bull.setVelX(20);
        bull.setVelY(20);
        ArrayList<Bullet> bullets = new ArrayList<>();
        bullets.add(bull);
        ship.setBullets(bullets);
        game.setShip(ship);
        game.bulletUpdate(2);
        assertEquals((game.getShip().getBullets().get(0).getDirX() * 2) * 57 + 30,
            game.getShip().getBullets().get(0).getxValue());
        assertEquals((game.getShip().getBullets().get(0).getDirY() * 2) * 57 + 30,
            game.getShip().getBullets().get(0).getY());
    }

    @Test
    void asteroidUpdate() {
        asteroid.setX(10);
        asteroid.setY(10);
        asteroid.setVelX(10);
        asteroid.setVelY(10);
        game.getAsteroids().add(asteroid);
        double x = asteroid.getxValue();
        double y = asteroid.getY();
        game.astUpdate(2);
        assertEquals((game.getAsteroids().get(0).getVelY() * 2) * 10 + y,
            game.getAsteroids().get(0).getY());
        assertEquals((game.getAsteroids().get(0).getVelX() * 2) * 10 + x,
            game.getAsteroids().get(0).getxValue());
    }

    @Test
    void testShipInitialization() {
        game.initShip(ship);
        assertEquals(420, ship.getxValue());
        assertEquals(280, ship.getY());
    }

    @Test
    void testWorldCreation() {
        game.createWorld();
        assertNotNull(game.getWorld());
    }

    @Test
    void warpAroundTest1() {
        ship.setX(AsteroidUI.WIDTH + 30);
        ship.setY(30);
        game.setAsteroids(as);
        game.setShip(ship);
        double x = ship.getxValue();
        ship.update(0);
        game.wrapAround(ship);
        assertEquals(x - AsteroidUI.WIDTH, game.getShip().getxValue());
    }

    @Test
    void warpAroundTest2() {
        ship.setX(-10);
        ship.setY(30);
        game.setAsteroids(as);
        game.setShip(ship);
        double x = ship.getxValue();
        ship.update(0);
        game.wrapAround(ship);
        assertEquals(x + AsteroidUI.WIDTH, game.getShip().getxValue());
    }

    @Test
    void warpAroundTest3() {
        ship.setX(30);
        ship.setY(AsteroidUI.HEIGHT + 30);
        game.setAsteroids(as);
        game.setShip(ship);
        double y = ship.getY();
        ship.update(0);
        game.wrapAround(ship);
        assertEquals(y - AsteroidUI.HEIGHT, game.getShip().getY());
    }

    @Test
    void warpAroundTest4() {
        as.add(asteroid);
        ship.setX(30);
        ship.setY(-10);
        game.setAsteroids(as);
        game.setShip(ship);
        double y = ship.getY();
        ship.update(0);
        game.wrapAround(ship);
        assertEquals(y + AsteroidUI.HEIGHT, game.getShip().getY());
    }

    @Test
    void initAstTest() {
        ArrayList<Asteroid> as = new ArrayList<>();
        as.add(asteroid);
        game.initAst(asteroid);
        assertEquals(as, game.getAsteroids());
    }

    @Test
    void testLevelInitial() {
        assertEquals(0, game.getLevel());
    }

    @Test
    void testLevelUp() {
        //There are maximum 5 levels
        for (int i = 1; i <= 5; i++) {
            game.newLevel(textureRegion);
            game.setAsteroids(new ArrayList<Asteroid>());
            assertEquals(i, game.getLevel());
        }
    }

    @Test
    void testGetterSize() {
        assertEquals(1, game.getSize());
    }

    @Test
    void testSetterSize() {
        game.setSize(2);
        assertEquals(2, game.getSize());
    }

    @Test
    public void testSplit() {
        game.split(asteroid, asteroid.getAsteroidRegion());
        Assertions.assertEquals(2, game.getAsteroids().size());
    }

    @Test
    public void testNotSplit() {
        asteroid.decSplitCount();
        asteroid.decSplitCount();
        game.getAsteroids().add(asteroid);
        game.split(asteroid, asteroid.getAsteroidRegion());
        Assertions.assertEquals(1, game.getAsteroids().size());
    }
}