package game.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import game.Bullet;
import game.Ship;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class BulletTest {
    private transient Ship ship;
    private transient Bullet bullet;
    private transient Bullet bullet2;

    @Mock
    private transient TextureRegion textureRegion;


    @BeforeEach
    void setUp() {
        ship = new Ship(textureRegion);
        bullet = new Bullet(ship.getxValue(), ship.getY(), textureRegion);
        bullet2 = mock(Bullet.class);
    }

    @Test
    void bulletVelocityTest() {
        bullet.setVelX(10);
        bullet.setVelY(20);
        assertEquals(bullet.getVelX(), 10);
        assertEquals(bullet.getVelY(), 20);
    }

    @Test
    void bulletLocationTest() {
        bullet.setX(44);
        bullet.setY(44);
        assertEquals(bullet.getY(), 44);
        assertEquals(bullet.getxValue(), 44);
        bullet.incX(4);
        bullet.incY(4);
        assertEquals(bullet.getY(), 48);
        assertEquals(bullet.getxValue(), 48);
    }

    @Test
    void bulletAngleTest() {
        bullet.setFaceAngle(20);
        bullet.setMoveAngle(40);
        assertEquals(bullet.getMoveAngle(), 40);
        assertEquals(bullet.getFaceAngle(), 20);
        bullet.incFaceAngle(5);
        bullet.incMoveAngle(5);
        assertEquals(bullet.getMoveAngle(), 45);
        assertEquals(bullet.getFaceAngle(), 25);

    }

    @Test
    void bulletOriginTest() {
        bullet.setDirX(37);
        bullet.setDirY(37);
        assertEquals(bullet.getDirX(), 37);
        assertEquals(bullet.getDirY(), 37);
    }

    @Test
    void bulletShapeTest() {
        float[] shape = {-6,7, -8,7, 9,7, 6,1, 3,3, 9,-7};
        bullet.setShape(new Polygon(shape));
        assertEquals(bullet.getShape().getVertices(), shape);
    }

    @Test
    void bulletBoundsTest() {
        Rectangle r = new Rectangle((int) bullet.getxValue() - 1,
            (int) bullet.getY() - 1, 1, 1);
        assertEquals(r, bullet.getBounds());
    }

    @Test
    void bulletAliveTest() {
        assertFalse(bullet.isAlive());
        bullet.setAlive(true);
        assertTrue(bullet.isAlive());
    }

    @Test
    void textureTest() {
        Mockito.when(bullet2.getBulletRegion()).thenReturn(textureRegion);
        assertEquals(textureRegion, bullet2.getBulletRegion());
    }
}

