package game.test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.anyDouble;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import game.Asteroid;
import game.Bullet;
import game.Collision;
import game.Game;
import game.Intersector;
import game.Ship;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CollisionTest {

    @Mock
    private transient Game game;
    @Mock
    private transient Ship ship;
    @Mock
    private transient Asteroid asteroid;
    @Mock
    private transient Intersector intersector;
    @Mock
    private transient Bullet bullet;

    private transient ArrayList<Bullet> bullets;

    private transient Collision collision;


    /**
     * Mocks initialization.
     */
    @BeforeEach
    public void setUp() {
        initMocks(this);
        ArrayList<Asteroid> asteroids = new ArrayList<>();
        bullets = new ArrayList<>();

        for (int i = 0;i < 10; i++) {
            asteroids.add(asteroid);
            bullets.add(bullet);
        }
        when(game.getShip()).thenReturn(ship);
        when(game.getAsteroids()).thenReturn(asteroids);
        when(ship.isAlive()).thenCallRealMethod();
        collision = new Collision(game, intersector);

    }

    @Test
    public void testAsteroidVsShip() {
        Ship s = new Ship(new TextureRegion());
        Assertions.assertEquals(2, s.getLives());
        collision = new Collision(game, intersector, s);
        when(intersector.intersect(any(),any())).thenReturn(true);
        collision.checkCollisions();
        Assertions.assertEquals(-3, s.getLives());
    }

    @Test
    public void testNoHit() {
        when(intersector.intersect(any(),any())).thenReturn(false);
        collision.checkCollisions();
        verify(ship, times(0)).setAlive(anyBoolean());
    }

    @Test
    public void testBulletVsAsteroid() {
        when(ship.getBullets()).thenReturn(bullets);
        when(intersector.intersect(any(),any())).thenReturn(true);
        collision.checkCollisions();
        verify(game, times(8)).addScore(anyDouble());
    }

    @Test
    public void testBulletVsAsteroidNoHit() {
        when(ship.getBullets()).thenReturn(bullets);
        when(intersector.intersect(any(),any())).thenReturn(false);
        collision.checkCollisions();
        verify(game, times(0)).addScore(anyDouble());
    }
}
