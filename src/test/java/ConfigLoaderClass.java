import static org.junit.jupiter.api.Assertions.assertEquals;

import gui.ConfigLoader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ConfigLoaderClass {

    @BeforeAll
    public void setUp() {
        ConfigLoader.readValues("testConfigLoader.properties");
    }

    @Test
    public void testUrl() {
        assertEquals("testURL", ConfigLoader.getTestDbUrl());
    }

    @Test
    public void testUser() {
        assertEquals("testUser", ConfigLoader.getTestDbUser());
    }

    @Test
    public void testPassword() {
        assertEquals("testPassword", ConfigLoader.getTestDbPassword());
    }

    @Test
    public void realPassword() {
        assertEquals("testRealPassword", ConfigLoader.getdBPassword());
    }

    @Test
    public void testRealUser() {
        assertEquals("testRealUser", ConfigLoader.getdBUser());
    }

    @Test
    public void testRealUrl() {
        assertEquals("testRealURL", ConfigLoader.getdBUrl());
    }
}
