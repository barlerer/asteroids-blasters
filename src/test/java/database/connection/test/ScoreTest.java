package database.connection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import database.connection.Score;
import org.junit.jupiter.api.Test;

public class ScoreTest {

    @Test
    public void constructorWithPoints() {
        Score score = new Score(5);
        assertEquals(5, score.getPoints());
    }

    @Test
    public void constructorWithoutPoints() {
        Score score = new Score();
        assertEquals(0, score.getPoints());
    }

    @Test
    public void setter() {
        Score score = new Score();
        score.setPoints(5);
        assertEquals(5, score.getPoints());
    }

    @Test
    public void getterNickname() {
        Score score = new Score(5, "Test");
        assertEquals("Test", score.getNickname());
    }
}
