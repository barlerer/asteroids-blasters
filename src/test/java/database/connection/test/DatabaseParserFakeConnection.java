package database.connection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import database.connection.DatabaseParser;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DatabaseParserFakeConnection {

    @Mock
    transient ResultSet resultSet;

    @InjectMocks
    transient DatabaseParser databaseParser;

    /**
     * Setup for the test suite.
     * @throws SQLException Will never get thrown, it's a mock
     */
    @BeforeAll
    public void setUp() throws SQLException {
        databaseParser = new DatabaseParser();
        initMocks(this);
        when(resultSet.next()).thenThrow(SQLException.class);
    }

    @Test
    public void failScoreParsing() {
        assertEquals(0, databaseParser.parseScores(resultSet).size());
    }
}
