package database.connection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import database.connection.DatabaseAccess;
import database.connection.Score;
import database.connection.User;
import java.sql.Connection;
import java.sql.SQLException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DatabaseAccessFakeConnection {

    transient User user;

    @InjectMocks
    transient DatabaseAccess databaseAccess;

    @Mock
    transient Connection connection;

    /**
     * Mock, will never throw SQLEception.
     * @throws SQLException will never happen
     */
    @BeforeAll
    public void setUp() throws SQLException {
        databaseAccess = new DatabaseAccess();
        databaseAccess.setConnection(connection);
        initMocks(this);
        user = new User("test@test.com", "1234");
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);
    }

    @Test
    public void failUserLoginSqlError() {
        assertFalse(databaseAccess.userLogin(user));
    }

    @Test
    public void failSaveScores() {
        Score score = new Score();
        assertFalse(databaseAccess.saveScore(user,"test", score));
    }

    @Test
    public void failGetUserId() {
        assertEquals(-1,databaseAccess.getUserId(user));
    }

    @Test
    public void failGetScores() {
        databaseAccess.getScores(user);
        assertEquals(0, user.getScores().size());
    }
}
