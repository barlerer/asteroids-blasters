package database.connection.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import database.connection.Hashing;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class HashingTest {

    private static Hashing hashing;

    @BeforeAll
    public void setUp() {
        this.hashing = new Hashing();
    }

    @Test
    public void testCryptDechyper() {
        String password = "HelloTest";
        String hashedPassword = hashing.crypt(password);
        assertTrue(hashing.verifyCrypt(password, hashedPassword));
    }

    @Test
    public void testCryptDechyperDiffPass() {
        String password = "HelloTest";
        String password2 = "Hellotest";
        String hashedPassword = hashing.crypt(password);
        assertFalse(hashing.verifyCrypt(password2, hashedPassword));
    }

    @Test
    public void testCryptDechyperNull() {
        String password = "HelloTest";
        String password2 = null;
        String hashedPassword = hashing.crypt(password);
        assertFalse(hashing.verifyCrypt(password2, hashedPassword));
    }

    @Test
    public void testCryptNull() {
        String password = null;
        assertNull(hashing.crypt(password));
    }

    @Test
    public void testCounstructor() {
        Hashing hashing = new Hashing();
        assertTrue(hashing instanceof Hashing);
    }

}
