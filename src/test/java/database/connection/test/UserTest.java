package database.connection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import database.connection.Hashing;
import database.connection.Score;
import database.connection.User;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class UserTest {

    private static final String email = "test@tudelft.nl";
    private static final String password = "123456";

    private transient User user;

    @BeforeEach
    public void setUp() {
        user = new User(email, password);
    }

    @Test
    public void constructor() {
        assertEquals(email, user.getEmail());
        assertEquals(password, user.getPassword());
        assertFalse(user.isRegistered());
    }

    @Test
    public void emailSetter() {
        User user = new User(email, password);
        user.setEmail("totest@testing.com");
        assertEquals("totest@testing.com", user.getEmail());
    }

    @Test
    public void setPasswordSetter() {
        User user = new User(email, password);
        user.setPassword("123");
        assertEquals("123", user.getPassword());
    }

    @Test
    public void emptyScores() {
        assertEquals(0, user.getScores().size());
    }

    @Test
    public void setScores() {
        Score score = new Score(3);
        ArrayList<Score> scores = new ArrayList();
        scores.add(score);
        user.setScores(scores);
        assertEquals(scores, user.getScores());
    }

    @Test
    public void addScores() {
        Score score = new Score(3);
        user.addScore(score);
        assertEquals(score, user.getScores().get(0));
    }

    @Test
    public void setRegistered() {
        user.setRegistered(true);
        assertTrue(user.isRegistered());
    }

    @Test
    public void cryptPassword() {
        user.cryptPassword();
        assertNotEquals(password, user.getPassword());
        Hashing hashing = new Hashing();
        assertTrue(hashing.verifyCrypt(password,user.getPassword()));
    }
}
