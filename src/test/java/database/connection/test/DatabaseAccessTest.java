package database.connection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import database.connection.DatabaseAccess;

import database.connection.Hashing;
import database.connection.Score;
import database.connection.User;
import gui.ConfigLoader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DatabaseAccessTest {

    private static DatabaseAccess databaseAccess;
    private static Connection connection;
    private static ResultSet resultSet;
    private static Hashing hashing;
    private static User user;
    private static String userDb;
    private static String password;
    private static String url;

    /**
     * Testing connection to real database.
     */
    @BeforeAll
    public void setUp() {
        databaseAccess = new DatabaseAccess();
        hashing = new Hashing();
        ConfigLoader.readValues("config.properties");
        userDb = ConfigLoader.getTestDbUser();
        password = ConfigLoader.getTestDbPassword();
        url = ConfigLoader.getTestDbUrl();
        user = new User("test@gmail.com", "123456");
        connection = databaseAccess.connectToDB(url, userDb, password);
    }

    /**
     * Closing the connection after all testing are done.
     */
    @AfterAll
    public void finishTesting() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the schema everytime for new userDb environment.
     */
    //Suppressed Because of a bug of PMD, even when statement is closed, error appears.
    @SuppressWarnings("PMD.CloseResource")
    @BeforeEach
    public void creatingTable() {
        try {
            Statement st = connection.createStatement();
            st.addBatch("\n"
                    + "CREATE TABLE `users` (\n"
                    + "\t`id` int NOT NULL AUTO_INCREMENT,\n"
                    + "\t`email` varchar(200) NOT NULL UNIQUE,\n"
                    + "\t`password` varchar(100) NOT NULL,\n"
                    + "\t`registration_date` TIMESTAMP DEFAULT "
                    + "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n"
                    + "\tPRIMARY KEY (`id`)\n);");
            st.addBatch("CREATE TABLE `scores` (\n"
                    + "\t`score_id` int NOT NULL AUTO_INCREMENT,\n"
                    + "\t`user_id` int NOT NULL,\n"
                    + "\t`score` int NOT NULL,\n"
                    + "\t`nickname` varchar(100) NOT NULL DEFAULT 'Unknown',\n"
                    + "\tPRIMARY KEY (`score_id`)\n);");
            st.addBatch("ALTER TABLE `scores` ADD CONSTRAINT `scores_fk0` "
                    + "FOREIGN KEY (`user_id`) REFERENCES `users`(`id`);");
            st.executeBatch();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO users (email, password) "
                            + "VALUES ('test@gmail.com', ?);");
            preparedStatement.setString(1, hashing.crypt("123456"));
            preparedStatement.execute();
            st.addBatch("INSERT INTO scores (user_id, score, nickname) "
                    + "VALUES ('1', '500', 'testNick Name');");
            st.executeBatch();
            st.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Clearing of the database.
     */
    @AfterEach
    public void tearDown() {
        try {
            connection.createStatement().execute("DROP TABLE IF EXISTS scores;");
            connection.createStatement().execute("DROP TABLE IF EXISTS users;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConnection() {
        try {
            assertTrue(connection.isValid(60));
        } catch (SQLException e) {
            fail("Could not connect to the Database");
        }
    }

    @Test
    public void testUsersTable() {
        try {
            resultSet = connection.createStatement().executeQuery("SELECT * FROM users;");
            resultSet.next();
            assertEquals(resultSet.getString(2), "test@gmail.com");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testBadConnection() {
        DatabaseAccess badDatabaseAccess = new DatabaseAccess();
        assertNull(badDatabaseAccess.connectToDB(null, null, null));
    }

    @Test
    public void testScores() {
        databaseAccess.getScores(user);
        assertEquals(1, user.getScores().size());
        assertEquals(500, user.getScores().get(0).getPoints());
    }

    @Test
    public void testNoScores() {
        User user = new User("test1@gmail.com", "123456");
        databaseAccess.getScores(user);
        assertEquals(0, user.getScores().size());
    }

    @Test
    public void testOrderOfScores() {
        createPlentyScores();
        databaseAccess.getScores(user);
        assertEquals(5, user.getScores().size());
        assertTrue(checkIfSorted(user.getScores()));
    }

    @Test
    public void testScoresWhenLogin() {
        createPlentyScores();
        databaseAccess.userLogin(user);
        assertEquals(5, user.getScores().size());
        assertTrue(checkIfSorted(user.getScores()));
    }

    @Test
    public void testGetUserID() {
        assertEquals(1, databaseAccess.getUserId(user));
    }

    @Test
    public void testGetUserIdBadUser() {
        User userBad = new User("Not@real.net", "123");
        assertEquals(-1, databaseAccess.getUserId(userBad));
    }

    @Test
    public void testSaveScore() {
        Score score = new Score(420);
        databaseAccess.getScores(user);
        int scoresBeforeSave = user.getScores().size();
        assertTrue(databaseAccess.saveScore(user, "TestNick", score));
        databaseAccess.getScores(user);
        assertEquals(scoresBeforeSave + 1, user.getScores().size());
    }

    private boolean checkIfSorted(ArrayList<Score> scores) {
        for (int i = 1; i < scores.size(); i++) {
            if (scores.get(i - 1).getPoints() < scores.get(i).getPoints()) {
                return false;
            }
        }
        return true;
    }

    private void createPlentyScores() {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO scores (user_id, score, nickname) "
                            + "VALUES (?, ?, ?);");
            ps.setInt(1, 1);
            ps.setInt(2, 501);
            ps.setString(3, "TestNickname");
            ps.addBatch();
            ps.setInt(2, 5018);
            ps.addBatch();
            ps.setInt(2, 90000);
            ps.setString(3, "TopDog");
            ps.addBatch();
            ps.setInt(2, 12);
            ps.setString(3, "LowDog");
            ps.addBatch();
            ps.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saveScoreBadUser() {
        Score score = new Score();
        assertFalse(databaseAccess.saveScore(null, "Test", score));
        assertFalse(databaseAccess.saveScore(user, null, score));
        assertFalse(databaseAccess.saveScore(user, "Test", null));
    }

    @Test
    public void testTopScores() {
        createPlentyScores();
        List<Score> scores = databaseAccess.getTopScores();
        assertEquals(90000, scores.get(0).getPoints());
        assertEquals("TopDog", scores.get(0).getNickname());
        assertEquals("LowDog", scores.get(scores.size() - 1).getNickname());
        assertTrue(checkIfSorted(user.getScores()));
    }
}