# Meeting 1


## Points of action
> The items below are things you should look into after the meeting.
During the meeting you can divide (some of) the work between the team members, so that everybody has something to do afterwards.
Denote this with a name between brackets after the point of action.

 >Write down the following
> - what gets discussed
> - which choices get made
> - who takes on certain tasks
